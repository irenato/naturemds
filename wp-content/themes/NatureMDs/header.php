<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115897164-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115897164-1');
</script>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
	
    <title> <?php wp_title( '-', true, 'right' ); bloginfo( 'name' ); ?> </title>
      
	<?php /* <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /> */ ?>
    
    <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/images/favicon.ico" />	  
    
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/jquery.fancybox.css" />
	<?php wp_head(); ?>    

<?php /* 
<meta name="viewport" content="initial-scale=1, maximum-scale=1">

<meta property="og:url" content="<?php bloginfo('url'); echo $_SERVER["REQUEST_URI"]; ?>" />
<meta property="og:title" content="<?php wp_title( '-', true, 'right' ); bloginfo( 'name' ); ?>" />
<meta property="og:type" content="" />
 */ ?>
<?php 
$image_25_src = get_template_directory_uri().'/images/logotip.jpg';  $logo_src = $image_25_src;
$attach_id = 0;
$header_image_2 = get_theme_mod('header_image_data');  
	if($header_image_2) { if($header_image_2->attachment_id) {
	$attach_id = $header_image_2->attachment_id;
	$thumb_arr2 = wp_get_attachment_image_src($attach_id, '');  $logo_src = $thumb_arr2[0];
	} }
/*if( (is_single() or is_page()) and has_post_thumbnail() ) {
$attach_id = get_post_thumbnail_id();
}*/
if($attach_id) {
$thumb_arr25 = wp_get_attachment_image_src($attach_id, '');
$image_25_src = $thumb_arr25[0];
}
?>
<meta property="og:image" content="<?php echo $image_25_src; ?>" />

<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url') ?>/images/apple-icon.png">
	    
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style-ie.css" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style-ie.css" />
<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style-ie.css" />
<![endif]-->  

<?php /* Активний пункт меню для всіх типів матеріалів і таксономій */ ?>
<style type="text/css">
/* Активний пункт */
/*.top_menu ul.menu > .current-menu-item > a,
.top_menu ul.menu > .current-menu-parent > a,
<?php 
$types_arr = get_post_types( array('public' => true ) );  unset($types_arr['attachment']); 
$taxo_arr = get_taxonomies(array('public' => true));  unset($taxo_arr['post_format'], $taxo_arr['post_tag']); 
$types_taxo_arr = array_merge( $types_arr, $taxo_arr );
foreach ($types_taxo_arr as $p_type ) : ?>
.top_menu ul.menu > .current-<?php echo $p_type ?>-parent > a,
.top_menu ul.menu > .current-<?php echo $p_type ?>-ancestor > a,
<?php endforeach; ?>
.top_menu ul.menu > .current-menu-ancestor > a
{
	color: #FFF !important;
	background: #555 !important;
}*/
</style>   	

</head>


<body <?php body_class(); ?>>

<?php /*  <div class="inner_body"> </div> */ ?>


<?php /* 
<div class="header_1">  
<div class="header_2"> </div>
<div class="menu_fon1"> </div>
<?php if ( is_front_page() ) { ?> <div class="header_34"> </div> <?php } ?>
</div>
 */ ?>


	<!-- BEGIN: wrapper -->    
	<div class="wrapper" id="main-wrapper">
	

		<!-- BEGIN: main content -->         
	<section id="main-content"> 
 

 
         
	 <header id="top" class="fix_menu">
     
     <div class="top-conteiner">



       </div> <!-- top-conteiner -->



<div class="main-menu">
	<div class="wrapper-cont">
	<div class="block logo" id="header-logo">
	  <a class="log_img" href="<?php bloginfo('url'); ?>"><img src="<?php echo $image_25_src; ?>" alt="Logo" /></a>
	</div>
	  <?php  /* other links ... */  ?>
	  <div id="menu1" class="top_menu"> <?php wp_nav_menu( array( 'theme_location' => 'm1', 'fallback_cb' => false ) ); ?></div>

	  <?php  /* Показати малюнки в пунктах меню - інструкція у front-page.php */  ?>
	  <div class="hed-right">
	  <div class="search-hed">
		<div class="wrap-search">
			<?php if(!function_exists ('dynamic_sidebar') || !dynamic_sidebar('Search_top')) : ?>
		  <?php endif; ?>
		</div>
	  </div>
	  <div class="social-hed">
		  <?php if(!function_exists ('dynamic_sidebar') || !dynamic_sidebar('Social')) : ?>
		  <?php endif; ?>
	  </div>
    	 <a target="_blank" class="login" href="<?php echo get_option('news_option3'); ?>">Register | Login</a>
    	 <a target="_blank" class="login mob_reg" href="<?php echo get_option('news_option3'); ?>">Register</a>
	  </div>
	  <a class="menu-hamb" id="menu1-menu-hamb"></a>
	</div>
</div>     
     



</header>        




    
<?php if(is_front_page()) { ?> 		
		<div class="block-top-hed">
		<?php $thumbnail_id = get_post_thumbnail_id($post->ID); 
		$main_img_arr2 = wp_get_attachment_image_src( $thumbnail_id, ''); ?>
			<?php  // Multi Post Thumbnails 
    $thumb = '';
	if ( has_post_thumbnail() ) { $thumb = get_the_post_thumbnail( $post->ID, 'slider-img' ); }  
		   if (class_exists('MultiPostThumbnails')) : 
	if ( MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'thumbnail-feat', NULL) ) { 
$thumb = MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'thumbnail-feat', NULL);

$thumb_url = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'thumbnail-feat');

	}
			endif;			
			?> 
 
			<div style="background: url('<?php echo $main_img_arr2[0] ?>') no-repeat  center center;" class="wrapper-cont">
				<div class="top-hed-text">
					<?php if(!function_exists ('dynamic_sidebar') || !dynamic_sidebar('text_hed_top')) : ?>
					<?php endif; ?>
				<div class="wrap-btn-in">
					<?php echo get_post_meta($post->ID, 'page_book_session', true); ?>
				</div>
				</div>
			</div>
			<div style="background: url('<?php echo $thumb_url ?>') no-repeat  center center;" class="wrapper-cont top-mob">
				<div class="top-hed-text">
					<?php if(!function_exists ('dynamic_sidebar') || !dynamic_sidebar('text_hed_top_mob')) : ?>
					<?php endif; ?>
				</div>
			</div>
		</div>	
				<div class="page-content">  
 <?php } else {?>    
 				<div class="page-content wrapper-cont">  
<?php } ?> 


