<?php get_header(); ?>


<div class="page no_column not_found blog">
    
	 <div class="content">
      
	
    <div class="big-text">Something went wrong.</div>
            
    <div class="maine entry-content page_404">
	<p>We weren't able to find the requested page.</p>
		<p>It has either been moved or doesn't exist anymore.</p>
		<a class="btn-session" href="/">Back to home page</a> 
    </div>
          
    </div>      
 
  
</div>


<?php /* 
<!-- переадресація -->
<script type="text/javascript">
<!-- 
function GoHom(){
 location="<?php bloginfo('url'); ?>"; 
}
setTimeout( 'GoHom()', 100 ); 
//--> 
</script>
 */ ?>

<?php get_footer(); ?>