<script type="text/javascript">
    function search_inp_check() {
        var inp_search = document.getElementById("s");
        // var inp_val = inp_search.value;
        // alert(inp_search.className);
        if ( (inp_search.value.length < 2) || (inp_search.className.indexOf("open-search") == -1) ) { return false; }
    }


    function do_ajax_search(s_input) {
        var s_input_val = s_input.value;
        var s_block = document.getElementById("search_ajax_suggest");
        if(s_input_val.length == 0) { var style_2 = 'block'; } else { var style_2 = 'none'; }
        s_block.children[1].style.display = style_2;

        var s_suggest_block = s_block.children[0];
        if(s_input_val.length > 2) {
            // alert(s_input_val);
            ajax_prepare_html(s_suggest_block);  <?php /* ф-ія ajax_prepare_html() - в head */ ?>
            new Ajax.Updater( page_temp.id, '<?php bloginfo('url'); echo '/?s='; ?>' + s_input_val, {
                method: 'post',
                parameters: {search_ajax_request: s_input_val}, // {id: '273', name_spisok: 'spisok25'}
                onComplete:
                    function() {
                        page_replace_new(s_suggest_block);  <?php /* ф-ія page_replace_new() - в head */ ?>
                    }
            } );
        }
        else { s_suggest_block.innerHTML = ''; }
    }


    function show_ajax_search_block(par) {
        var s_block = document.getElementById("search_ajax_suggest");
        if(par == 'hide') { var style_1 = 'none'; } else { var style_1 = 'block'; }
        setTimeout(function() { s_block.style.display = style_1; }, 200) ;
    }


    jQuery(document).ready(function($) {
        width_15 = $("#s").parent().width();  // $(this).width()
        width_16 = $("#s").css("max-width");
        if(window.innerWidth < 781) {
            $("#close_search").click(function () {
                var height_my_header = $(".main-menu .wrapper-cont").css("height");

                if (height_my_header == "100px") {
                    $(".main-menu .wrapper-cont").css('height', 'auto');
                }
                else {
                    // $(".main-menu .wrapper-cont").animate({'height':100}, 10);
                    $(".main-menu .wrapper-cont").animate({
                        height: "100px"
                    }, 300, function () {
                        // Animation complete.
                    });
                }
            });
        }

        $("#searchsubmit").click(function(){

            if(window.innerWidth < 781) {
                var height_my_header = $(".main-menu .wrapper-cont").css("height");

                if (height_my_header == "100px") {
                    $(".main-menu .wrapper-cont").css('height', 'auto');
                }
                else {
                    // $(".main-menu .wrapper-cont").animate({'height':100}, 10);
                    $(".main-menu .wrapper-cont").animate({
                        height: "100px"
                    }, 300, function () {
                        // Animation complete.
                    });
                }
            }


            var inp_val = document.getElementById("s").value.length;

            if(window.innerWidth > 781) {
                if (!$("#s").hasClass('open-search')) {
                    $("#s").animate({ width: width_16, 'padding-top': 8+'px', 'padding-bottom': 8+'px', 'padding-left':15+'px', 'padding-right':3+'px', 'margin-top': -5+'px'});
                    var s_block = $("#search_ajax_suggest");

                    setTimeout(function() { s_block.slideDown(); $("#s").addClass('open-search'); }, 400) ;
                    $("#s").css('background-color', '#ffffff');
                    $("#s").css('border', '1px solid #ccc');
                    $("#s").attr('placeholder', 'Search consultants, conditions or health blog');

                }
                else{
                    if ((inp_val)<2){
                        var s_block = $("#search_ajax_suggest");

                        s_block.slideUp();
                        setTimeout(function() {
                            $('#s').attr('placeholder', '').animate({height: 19+ 'px', width: 0 + 'px', 'padding-top': 3+'px', 'padding-bottom':3+'px', 'margin-top': 0+'px', 'border-width': 0+'px'});  $("#s").removeClass('open-search');
                        }, 400);

                        $("#s").val('');
                    }
                }
            }
            else{  //if window.width<=780

                if (!$("#s").hasClass('open-search')) {
                    $(".wrap-search > form").animate({height: 450+ 'px', 'border-bottom-width': 1+'px'});
                    var s_block = $(".search-form input[type='text']");
                    var s_block2 = $("#search_ajax_suggest");
                    setTimeout(function() { s_block.slideDown();  $("#s").addClass('open-search'); $(".close_search").show(); s_block2.slideDown(); $("#s").addClass('open-search'); }, 400) ;

                    $("#s").attr('placeholder', 'Search consultants, conditions or health blog');


                }
                else{
                    if ((inp_val)<2){
                        var s_block = $(".search-form input[type='text']");
                        var s_block2 = $("#search_ajax_suggest");

                        setTimeout(function() { s_block2.hide();
                        }, 400);
                        setTimeout(function() {s_block.hide();
                            $("#s").removeClass('open-search');
                        }, 400);
                        $("#s").val('');
                        $(".wrap-search > form").animate({height: 0+ 'px'});
                    }
                }


            }



        });

        $(document).click(function (e){ // событие клика по веб-документу

            var div = $("#search-form"); // тут указываем ID элемента
            var div_search = $("#close_search");
            if (!div.is(e.target) // если клик был не по нашему блоку
                && (div.has(e.target).length === 0) || (div_search.is(e.target))) { // и не по его дочерним элементам
                if(window.innerWidth > 781) {
                    var s_block = $("#search_ajax_suggest");
                    s_block.slideUp();
                    setTimeout(function() {
                        $('#s').attr('placeholder', '').animate({width: 0 + 'px', 'padding-top': 3+'px', 'padding-bottom':3+'px', 'margin-top': 0+'px', 'border-width': 0+'px'});
                    }, 400);
                    $("#s").removeClass('open-search');
                    //$("#s").val('');
                    $("#s").addClass('not-search');
                } else {
                    var s_block = $(".search-form input[type='text']");
                    var s_block2 = $("#search_ajax_suggest");

                    setTimeout(function() { s_block2.hide(); $(".wrap-search > form").animate({height: 0+ 'px'});
                    }, 400);

                    setTimeout(function() {s_block.hide();
                        $("#s").removeClass('open-search');
                    }, 400);
                    $("#s").val('');
                }

                if(window.innerWidth < 781) {
                    var display_my_header = $("#search_ajax_suggest").css("display");
                    if (display_my_header == "block") {
                        $(".main-menu .wrapper-cont").animate({
                            height: "100px"
                        }, 300, function () {
                            // Animation complete.
                        });
                    }
                    else {
                        // // $(".main-menu .wrapper-cont").animate({'height':100}, 10);
                        // $(".main-menu .wrapper-cont").animate({
                        //     height: "100px"
                        // }, 300, function () {
                        //     // Animation complete.
                        // });
                    }
                }
            }
        });
    });

</script>
<form method="get" action="<?php bloginfo('url'); ?>/" onsubmit="return search_inp_check()" > 	<div id="search-form" class="search-form">
<input type="text" name="s" id="s" placeholder=""  onkeyup="do_ajax_search(this)" value="" autocomplete="off" />

<?php /* <button type="button" onclick="this.form.submit()" name="searchsubmit" id="searchsubmit" title="<?php _e('Search') ?>"><span><?php _e('Search') ?></span></button> */ ?>
 
<button type="submit" id="searchsubmit"> <span><?php _e('Search') ?></span> </button> 

<div id="search_ajax_suggest" style="display:none;">
<div class="search_aj"></div>
<div class="perfect"><?php include 'search-perfect.php'; /* *** search perfect list *** */ ?></div> 
<span id="close_search" class="close_search"></span>
</div>

	</div>
</form>
