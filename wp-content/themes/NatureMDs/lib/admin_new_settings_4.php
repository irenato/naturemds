<?php // 
add_action('admin_init', 'admin_new_read_settings_4');

function admin_new_read_settings_4() {
	register_setting(
		'reading',                 // settings page
		'site_add_settings_4',          // option name
		'admin_setti_validate_49'  // validation callback
	);	
/* 
	add_settings_field(
		'home_title_1',      // id
		'___ Categories on Home',          // setting title
		'home_title_1_input',    // display callback
		'reading',                 // settings page
		'default'                  // settings section
	);
 */
	add_settings_field(
		'home_blog_cat_ids',    
		__('Home blog categories ids ("category")'),   
		'home_blog_cat_ids_input', 
		'reading',    
		'default'   
	);	
	add_settings_field(
		'blog_main_cat_ids',    
		__('Main blog page - cat id ("category")'),   
		'blog_main_cat_ids_input', 
		'reading',    
		'default'   
	);	
	add_settings_field(
		'home_health_cat_ids',    
		__('Home What we treat cat ids ("health-cat")'),   
		'home_health_cat_ids_input', 
		'reading',    
		'default'   
	);	

}
// Display and fill the form field
function home_title_1_input() {
	$options = get_option('site_add_settings_4');	$value = $options['home_title_1'];
	echo '<input type="text" id="home_title_1" name="site_add_settings_4[home_title_1]" value="'.$value.'" size="60" />  E.g., 1,4,27';
}

function home_blog_cat_ids_input() {
	$options = get_option('site_add_settings_4');	$value = $options['home_blog_cat_ids'];
	echo '<input type="text" id="home_blog_cat_ids" name="site_add_settings_4[home_blog_cat_ids]" value="'.$value.'" size="40" />  E.g., 1,4,27';
}

function blog_main_cat_ids_input() {
	$options = get_option('site_add_settings_4');	$value = $options['blog_main_cat_ids'];
	echo '<input type="text" id="blog_main_cat_ids" name="site_add_settings_4[blog_main_cat_ids]" value="'.$value.'" size="40" />  E.g., 27';
}	

function home_health_cat_ids_input() {
	$options = get_option('site_add_settings_4');	$value = $options['home_health_cat_ids'];
	echo '<input type="text" id="home_health_cat_ids" name="site_add_settings_4[home_health_cat_ids]" value="'.$value.'" size="40" />  E.g., 1,4,27';
}	

// Validate user input
function admin_setti_validate_49( $input ) {	
	$valid = $input;
	return $valid;
} 
?>