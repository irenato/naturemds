<?php WOW_Product_List_Session::view_mode_change(); ?>

<?php $taxo = $wp_query->tax_query->queries[0]['taxonomy'];
if($taxo) { $taxo_2 = $taxo; } 
elseif($_REQUEST['par']) { $taxo_2 = $_REQUEST['par']; }
$tax_parameters = WOW_Product_List_Func::get_tax_parameters($taxo_2);

if($_REQUEST['par']) {
	$products_type_arr = WOW_Product_List_Func::get_front_products_type_arr();
	$p_type = $_REQUEST['par'];
	$tax_parameters['orderby'] = $products_type_arr[$p_type]['args']['orderby'];
	$tax_parameters['order'] = $products_type_arr[$p_type]['args']['order'];
}

$options_5 = get_option('wow_settings_arr');
?>


   <div class="toolbar title_content">
<form name="sorting_form" id="sorting_form_co" class="toolbar_form" method="GET" action="">

<?php if($_REQUEST['par']) { ?>
<input type="hidden" name="par" value="<?php echo $_REQUEST['par'] ?>" />
<?php } ?>

<?php 
if(!$options_5['wow_view_mode_one']) : ?>
<div class="view_mode">
<label class="lab view_m"><?php _e('View mode') ?>:</label>
<?php 
$view_mode = WOW_Product_List_Func::get_view_mode();

// if($view_mode == 'grid') { $v_title = __('List'); $view_mode_new = 'list'; } else { $v_title = __('Grid'); $view_mode_new = 'grid'; }

$view_mode_arr = array('grid' => __('Grid'), 'list' => __('List'));
$mode_icons_arr = array('grid' => 'fa fa-th', 'list' => 'fa fa-th-list');
?>
<?php foreach ($view_mode_arr as $v_key => $v_label) : 
$mode_id = 'mode-'.$v_key;
?>
   <a class="v_mode <?php echo $v_key; if($v_key == $view_mode) { echo ' act'; } ?>" <?php if($v_key != $view_mode) { ?>onclick="view_mode_change('<?php echo $v_key ?>')" <?php } ?>title="<?php echo $v_label ?>"><i class="<?php echo $mode_icons_arr[$v_key] ?>" aria-hidden="true"></i> <span><?php echo $v_label ?></span></a>
   <?php /* 
   <input type="radio" name="view_mode" id="<?php echo $mode_id ?>" value="<?php echo $v_key ?>" <?php if($v_key == $view_mode) { ?>checked="checked" <?php } else { ?>onchange="do_sort_form(this)" <?php } ?>/> 
   <label class="v_mode <?php echo $v_key; if($v_key == $view_mode) { echo ' act'; } ?>" for="<?php echo $mode_id ?>" title="<?php echo $v_label ?>"><span><?php echo $v_label ?></span></label>
    */ ?>
<?php endforeach; ?>
</div>
<?php endif; ?>
   
   
<?php 
/* Сайт. Сортування - сторінка категорії (index.php). */
   $attributes_sorting = WOW_Attributes_Front::attributes_sorting();
   
   $base_sorting = array();
   // $base_sorte = array ('title', 'date', 'views'); 
   $base_sorte = $tax_parameters['base_sorting'];
   // $sorting_labels_arr = array('title' => __('Title'), 'date' => __('Date'), 'comment_count' => __('Comments'), 'views' => __('Views count'));
   $sorting_labels_arr = WOW_Product_List_Func::get_sorting_labels_arr();
   foreach ($base_sorte as $sort_key) {
	   $base_sorting[] = array('code' => $sort_key, 'frontend_label' => $sorting_labels_arr[$sort_key]);
   }
	
	$sorting_arr = array_merge($base_sorting, $attributes_sorting); // print_r($sorting_arr);
	
	
	// $sort_code = 'title'; $sort_dir = 'asc'; // $sort_code = 'date'; $sort_dir = 'desc';
	$sort_code = $tax_parameters['orderby']; 
	$sort_dir = $tax_parameters['order'];
	$per_page = $tax_parameters['per_page'];
	if($_GET['orderby']) { $sort_code = $_GET['orderby']; }
	if($_GET['order']) { $sort_dir = $_GET['order']; }
	if($_GET['per_page']) { $per_page = $_GET['per_page']; }

// створити radio-кнопки для вибору к-сті товарів 
?>
<?php // print_r( $sorting_arr ); ?>
     
   
<div class="sorting">   

   				<label class="lab" for="posts_sorting_c"><?php _e('Sort by'); ?></label>    

        <div class="select_box">
        <i class="fa fa-caret-down"></i>
        <select name="orderby" id="posts_sorting_c" onchange="do_sort_form(this)"> 
        <?php foreach ($sorting_arr as $s_key => $sort_option) : ?>
      <option value="<?php echo $sort_option['code'] ?>" <?php if ($sort_option['code'] == $sort_code) { ?>selected="selected"<?php } ?>><?php echo $sort_option['frontend_label'] ?></option>
		<?php endforeach; ?>
		</select> <?php // onchange="document.forms.sorting_form.submit()" // ?>
        </div>
        
        
<?php // Варіант сортування "спочатку дешеві", "спочатку дорогі" // також розкоментувати javascript внизу ?>
<?php /*  
<input type="hidden" name="orderby" value="<?php echo $sort_code ?>" />
<?php 
$diff_sorting = array(
	'title' => array('label_asc' => __('Title asc 114'), 'label_desc' => __('Title desc 227')),
	'price' => array('label_asc' => __('Cheap first'), 'label_desc' => __('Expensive first'))
	);
$diff_sorting_keys = array_keys($diff_sorting);

$desc_sort_arr = array ('date', 'comment_count', 'views');

$sorting_arr_2 = array();
?>
        <?php foreach ($sorting_arr as $sort_option) :  
		$s_code = $sort_option['code'];
		if(in_array($s_code, $diff_sorting_keys)) { 
		$sorting_arr_2[] = array('code' => $s_code, 'dir' => 'asc', 'frontend_label' => $diff_sorting[$s_code]['label_asc']);
		$sorting_arr_2[] = array('code' => $s_code, 'dir' => 'desc', 'frontend_label' => $diff_sorting[$s_code]['label_desc']);
		}
		elseif(in_array($s_code, $desc_sort_arr)) {
		$sorting_arr_2[] = array('code' => $s_code, 'dir' => 'desc', 'frontend_label' => $sort_option['frontend_label']);
		}
		else {
		$sorting_arr_2[] = array('code' => $s_code, 'dir' => 'asc', 'frontend_label' => $sort_option['frontend_label']);
		}
		endforeach; ?>
        
      <div class="sort_block">  
		<?php foreach ($sorting_arr_2 as $sort_option) : ?>
        <div class="sort_item <?php echo $sort_option['dir'] ?>">
        <a <?php if (($sort_option['code'] == $sort_code and $sort_option['dir'] == $sort_dir) or ($sort_option['code'] == $sort_code and !in_array($sort_option['code'], $diff_sorting_keys))) { ?>class="act"<?php } else { ?>onclick="do_sort_form_2('<?php echo $sort_option['code'] ?>', '<?php echo $sort_option['dir'] ?>')"<?php } ?> title="<?php echo $sort_option['frontend_label'] ?>"><?php echo $sort_option['frontend_label'] ?></a>
    	</div>
		<?php endforeach; ?>
      </div>  
 */ ?>

        
        
    
<?php 
if($sort_dir == 'desc') { $sort_title = __('change to ASC'); } else { $sort_title = __('change to DESC'); }

$sort_dir_arr = array('asc' => __('ASC'), 'desc' => __('DESC'));
$dir_icons_arr = array('asc' => 'fa fa-chevron-up', 'desc' => 'fa fa-chevron-down');
?>
<div class="s_dir_block">
<?php foreach ($sort_dir_arr as $sort_key => $sort_label) : 
$sort_id = 'toolbar_sort-'.$sort_key;
?>
   <div class="s_dir <?php echo $sort_key ?>"<?php if($sort_key == $sort_dir) { ?> style=" display: none;"<?php } ?>>
   <input type="radio" name="order" id="<?php echo $sort_id ?>" value="<?php echo $sort_key ?>" <?php if($sort_key == $sort_dir) { ?>checked="checked" <?php } else { ?>onchange="do_sort_form(this)" <?php } ?>/> 
   <label class="sort_dir <?php echo $sort_dir ?>" for="<?php echo $sort_id ?>" title="<?php echo $sort_title ?>"><i class="<?php echo $dir_icons_arr[$sort_dir] ?>"></i> <span><?php echo $sort_dir ?></span></label>
   </div>   
<?php endforeach; ?>  
</div>
  
</div>

</form>

<script type="text/javascript">
function do_sort_form(elem) {
	var form_par = '';
	/* '_desc' */ if(elem.value == 'date' || elem.value == 'views') { document.getElementById("toolbar_sort-desc").checked = true; }
	if(document.getElementById('filter_form_co')) {   
	var filter_form = document.forms.filter_form;  // var filter_form = document.forms["filter_form"];
	filter_form.elements[elem.name].value = elem.value; // filter_form.elements['sorti[code]'].value = '9997';	
	/* '_desc' */ if(elem.value == 'date' || elem.value == 'views') { filter_form.order.value = 'desc'; }
	}
	else { form_par = 'sort_only'; }
	posts_filter(form_par);  // filter_form.submit();
}
<?php /* 
<?php // Варіант сортування "спочатку дешеві", "спочатку дорогі" ?>
function do_sort_form_2(orderby_val, order_val) {
	var form_par = '';
	
	var sort_form = document.forms.sorting_form;
	sort_form.orderby.value = orderby_val;
	var radio_order_asc = document.getElementById("toolbar_sort-asc");
	var radio_order_desc = document.getElementById("toolbar_sort-desc");
	if(order_val == 'asc') { radio_order_asc.checked = true; } else { radio_order_desc.checked = true; }
	
	if(document.getElementById('filter_form_co')) {   
	var filter_form = document.forms.filter_form;  
	filter_form.orderby.value = orderby_val;
	filter_form.order.value = order_val; 	
	}
	else { form_par = 'sort_only'; }
	
	posts_filter(form_par);  // filter_form.submit(); 
}
 */ ?> 
</script>
   </div> <!-- toolbar --> 
