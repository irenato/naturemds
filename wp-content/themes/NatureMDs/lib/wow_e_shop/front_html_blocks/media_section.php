<div class="images-box">
<?php // $product_type = get_post_meta($post->ID, 'product_type', true); 
// $con_main_prod_id = WOW_Attributes_Front::configurable_prod_default(); 
$post_id = $post->ID;
$post_id_gen = $post->ID;
if($con_main_prod_id and !has_post_thumbnail()) { $post_id_gen = $con_main_prod_id; }

if(!has_post_thumbnail($post_id) and wp_get_post_parent_id($post_id)) { $post_id_gen = wp_get_post_parent_id($post_id); }
?>


	<?php if ( has_post_thumbnail($post_id_gen) ) : ?> 
<?php 
$main_img_id = "product-view-image-".$post_id;
if (is_single()) { $img_size = 'main-img'; $onclick_action = 'onclick'; } 
else { $img_size = 'medium-img'; $onclick_action = 'onMouseOver'; }
	
$gallery_arr = WOW_Attributes_Front::image_gallery();

// $gal_mode = 2; // 0 - режим "заміни"; 1 - режим "суто Lightbox"; 2 - режим "ЛУПИ"; //
$options_5 = get_option('wow_settings_arr');
$gal_mode = $options_5['wow_gal_mode'];
// $id_edic = 3; // щоб в режимі "заміни" у слайд-шоу було тільки 1 фото 
$id_edic = '';  if($gal_mode == 1) { $id_edic = 1; }
?>

<?php $thumbnail_id = get_post_thumbnail_id($post_id); 
$img_arr24 = wp_get_attachment_image_src( $thumbnail_id, ''); ?>
<div class="main-img" id="<?php echo $main_img_id ?>"> 
<a 
<?php if(!is_single()) { ?>href="<?php the_permalink() ?>"
<?php } elseif( $gal_mode == 2 ) { ?>class="cloud-zoom" href="<?php echo $img_arr24[0] ?>" rel="useWrapper: false, showTitle: true, zoomWidth:'414', zoomHeight:'372', adjustY:0, adjustX:10"
<?php } elseif( $gallery_arr['slb_enab'] and $gallery_arr['main_img_key'] ) { ?>href="<?php echo $img_arr24[0] ?>" data-slb-internal="0" data-slb-active="1" data-slb-group="<?php the_ID(); echo $id_edic; ?>"<?php } ?>
>
<?php echo get_the_post_thumbnail($post_id_gen, $img_size); echo $gallery_arr['gallery_mode']; ?>
</a>
</div>
<?php /* <img title="Sample Title"> */ ?>
    
	<?php if($gallery_arr['image_gallery']) { /////////  ///////// /////////// image gallery
	$gallery_imgs = $gallery_arr['image_gallery'];  
	?>
 
    <div class="image_gallery<?php if($product_type == 'configurable') { ?> show_nav<?php } ?>">
    
<?php if (count($gallery_imgs) > 3) : ?>
 <div class="hslider-nav post-<?php echo $post_id ?> hslider-prev"> <i class="fa fa-chevron-left"></i> </div>
 <div class="hslider-nav post-<?php echo $post_id ?> hslider-next"> <i class="fa fa-chevron-right"></i> </div>
<?php endif; ?>

    <div class="gallery-slider post-<?php echo $post_id ?>">
    <ul>
	<?php foreach($gallery_imgs as $img_id) : ?>
    <?php $img_ss_full = wp_get_attachment_image_src($img_id, ''); $img_ss_main = wp_get_attachment_image_src($img_id, $img_size);  
	if(is_single()) { $attachment = get_post($img_id); $img_title = $attachment->post_excerpt; } ?>
    <li>
    <a 
<?php if(is_single() and $gallery_arr['slb_enab'] and $gal_mode == 1) { ?>href="<?php echo $img_ss_full[0] ?>" data-slb-internal="0" data-slb-active="1" data-slb-group="<?php the_ID() ?>1"
<?php } elseif( is_single() and $gal_mode == 2 ) { ?>class="cloud-zoom-gallery" href="<?php echo $img_ss_full[0] ?>" rel="smallImage: '<?php echo $img_ss_main[0] ?>', imgTitle: '<?php echo $img_title ?>'"
<?php } else { echo $onclick_action; ?>="change_main_img('<?php echo $img_ss_main[0] ?>', '<?php echo $img_ss_full[0] ?>', '<?php echo $post_id ?>', this)"<?php } ?>
	>
	<?php echo wp_get_attachment_image( $img_id, 'thumbnail' ) ?>
    </a>
    <?php /* onclick="change_main_img()"
	<img src="<?php echo $img_ss_thumb[0] ?>" width="<?php echo $img_ss_thumb[1] ?>" height="<?php echo $img_ss_thumb[2] ?>" /> */ 
	?>
    </li>
	<?php endforeach; ?>
    </ul>
    </div>
    </div>

<?php /* яваскрипт change_main_img() - в HEAD */ ?>

<?php /* jquery  */ ?>
<?php /* script jCarousel */ ?>
<?php /* 
<?php if (count($gallery_imgs) > $slider_count) : ?>
<?php $circular_par = 'false';  if( $gal_mode == 0 ) { $circular_par = 'true'; } ?>
<?php endif; ?>
  */ ?>   
<script type="text/javascript">
jQuery(document).ready(function($) {
		var slides_count = <?php echo count($gallery_imgs); ?>;  var count_42 = 1;
		var jcarousel = $('.gallery-slider.post-<?php echo $post_id ?>');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                if (width >= 440) { width = width / 6; count_42 = slides_count - 6; } 
				else if (width >= 350) { width = width / 5; count_42 = slides_count - 5; }
                else if (width >= 250) { width = width / 4; count_42 = slides_count - 4; }
				else { width = width / 3; count_42 = slides_count - 3; }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
				if(count_42 <= 0) { jcarousel.parent().addClass("no_slide_navi"); }
            })
            .jcarousel({ wrap: 'circular' });

        $('.hslider-prev.post-<?php echo $post_id ?>').jcarouselControl({ target: '-=1' });
        $('.hslider-next.post-<?php echo $post_id ?>').jcarouselControl({ target: '+=1' });
});
</script> 
    
	<?php } ////////// /////// //////////// ___ image gallery ?>
    

    <?php if (is_single()) : ?>
    <div style=" display: none;"><?php the_excerpt() ?></div> 
	<?php endif; ?>

    
    <?php else: ?>
    
    <?php /* <div class="no_feat_image"></div> */ ?>
    <div class="main-img inn"> <img src="<?php echo get_template_directory_uri() ?>/images/no_feat_image.png" class="no_feat" /> </div>
    
	<?php endif; // if has_post_thumbnail($post_id) ?>  





    <?php if (is_single()) : ?>
<?php if($product_type == 'configurable') { 
	$conf_ids_2 = get_post_meta($post->ID, 'configurable_ids', true); 
$conf_ids_4 = preg_replace('/[^0-9,]*/', '', $conf_ids_2); 
if($conf_ids_4) { $conf_ids_arr = explode(',', $conf_ids_4); $conf_ids_arr = array_unique($conf_ids_arr); }
$conf_args_2 = array (       
        'post_type'  => 'any',
		'post__in' => $conf_ids_arr,
		'posts_per_page'   => -1,
		'order' => 'ASC',	
		'orderby' => 'id',		
		'post_status' => 'publish'
    );

$confi_query = new WP_Query($conf_args_2);
    if( $confi_query->have_posts() ) { ?>
<div class="conf_items_imgs" style=" display: none;">    
	<?php while ($confi_query->have_posts()) : $confi_query->the_post(); ?>
<div><?php the_excerpt() ?></div>
	<?php endwhile; ?>
</div>
<?php }  wp_reset_query(); ?>
<?php } // ($product_type == 'configurable') ?>	
	<?php endif; ?>
          
</div>