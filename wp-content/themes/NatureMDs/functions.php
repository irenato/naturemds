<?php

add_theme_support( 'custom-header' );


function wp_tag_cloud_remove_title_attributes($return) {
        // This function uses single quotes
        return preg_replace('/\s*title\s*=\s*(["\']).*?\1/', '', $return);
    return $return;
}
add_filter('wp_tag_cloud', 'wp_tag_cloud_remove_title_attributes');

/*function search_excerpt_highlight() {
	$excerpt = get_the_excerpt();
	$keys = implode(&#8216;|&#8217;, explode(&#8216; &#8216;, get_search_query()));
	$excerpt = preg_replace(&#8216;/(&#8216; . $keys .&#8217;)/iu&#8217;, &#8216;<strong class="search-highlight">&#92;&#48;</strong>&#8217;, $excerpt);
	echo &#8216;<p>&#8217; . $excerpt . &#8216;</p>&#8217;;
}

function search_title_highlight() {
	$title = get_the_title();
	$keys = implode(&#8216;|&#8217;, explode(&#8216; &#8216;, get_search_query()));
	$title = preg_replace(&#8216;/(&#8216; . $keys .&#8217;)/iu&#8217;, &#8216;<strong class="search-highlight">&#92;&#48;</strong>&#8217;, $title);
	echo $title;
}*/

/*function set_newuser_cookie() {
    if (!isset($_COOKIE['newvisitor'])) {
        setcookie('newvisitor', 1, time()+1209600, COOKIEPATH, COOKIE_DOMAIN, false);
    }
}
add_action( 'init', 'set_newuser_cookie');*/

/// Функція для вставки меню 
register_nav_menus(
   array(
  'm1' => __('Main menu'),
  'm2' => __('Menu at the top'),
  'm3' => __('Footer menu'),
  'm4' => __('Menu blog'),
  'search_feature' => __('Search'),
  'search_halth' => __('Health'),
  // 'm_home_feature' => __('home feature'),
  // 'm_public_cats' => __('Menu with categories for publication'),
)
);
/// Функція для вставки сайдбарів 
function my_theme_sidebars_in() {
	register_sidebar(
		array(
			'id' => 'images_widget_in',
			'name' => __( 'New images widget' ),
			'description' => __( '' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',	
			'after_title'   => ''
		)
	);
	register_sidebar(
		array(
			'id' => 'Right_sidebar_blog',
			'name' => __( 'Right_sidebar_blog' ),
			'description' => __( '' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',	
			'after_title'   => '</h2>'
		)
	);
	register_sidebar(
		array(
			'id' => 'Latest_Blog',
			'name' => __( 'Latest_Blog' ),
			'description' => __( 'Latest_Blog' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h2>',	
			'after_title'   => '</h2>'
		)
	);
	register_sidebar(
		array(
			'id' => 'Social_single',
			'name' => __( 'Social_single' ),
			'description' => __( 'Social_single' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',	
			'after_title'   => ''
		)
	);
	register_sidebar(
		array(
			'id' => 'Search_top',
			'name' => __( 'Search_top' ),
			'description' => __( 'Search_top' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',	
			'after_title'   => ''
		)
	);
	register_sidebar(
		array(
			'id' => 'futer_social',
			'name' => __( 'futer_social' ),
			'description' => __( 'futer social' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<span class="widgettitle">',	
			'after_title'   => '</span>'
		)
	);
	register_sidebar(
		array(
			'id' => 'Social',
			'name' => __( 'social' ),
			'description' => __( 'social ico header' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<span class="widgettitle">',	
			'after_title'   => '</span>'
		)
	);
	register_sidebar(
		array(
			'id' => 'text_hed_top',
			'name' => __( 'Text header top' ),
			'description' => __( '' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',	
			'after_title'   => ''
		)
	);
	register_sidebar(
		array(
			'id' => 'text_hed_top_mob',
			'name' => __( 'Text header top mob' ),
			'description' => __( '' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',	
			'after_title'   => ''
		)
	);
	register_sidebar(
		array(
			'id' => 'left-sidebar',
			'name' => __( 'left-sidebar' ),
			'description' => __( 'Ліва колонка' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widgettitle">',	
			'after_title'   => '</h3>'
		)
	);	
	register_sidebar(
		array(
			'id' => 'right-sidebar',
			'name' => __( 'right-sidebar' ),
			'description' => __( 'Права колонка' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);
	register_sidebar(
		array(
			'id' => 'sidebar-contact',
			'name' => __( 'sidebar-contact' ),
			'description' => __( 'Контакти (телефон) в хедері' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s"> <i class="fa fa-phone-square"></i>',
			'after_widget'  => '</div>',
		)
	);
	register_sidebar(
		array(
			'id' => 'top-sidebar',
			'name' => __( 'top-sidebar' ),
			'description' => __( 'Верхній сайдбар (пошук)' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);
	register_sidebar(
		array(
			'id' => 'languagg',
			'name' => __( 'languagg' ),
			'description' => __( 'Languages' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);
	register_sidebar(
		array(
			'id' => 'home_sidebar_title',
			'name' => __( 'home_sidebar_title' ),
			'description' => __( 'Слайдер на головній - назва' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',	
			'after_title'   => '</h4>'
		)
	);		
	register_sidebar(
		array(
			'id' => 'futer_left',
			'name' => __( 'futer_left' ),
			'description' => __( ' ' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widgettitle">',	
			'after_title'   => '</span>'
		)
	);
	register_sidebar(
		array(
			'id' => 'futer_centr',
			'name' => __( 'futer_centr' ),
			'description' => __( '3 widgets - 3 columns' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widgettitle">',	
			'after_title'   => '</span>'
		)
	);
	register_sidebar(
		array(
			'id' => 'futer_right',
			'name' => __( 'futer_right' ),
			'description' => __( ' ' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widgettitle">',	
			'after_title'   => '</span>'
		)
	);
	register_sidebar(
		array(
			'id' => 'futer_copyright',
			'name' => __( 'futer_copyright' ),
			'description' => __( '' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);	
}

add_action( 'widgets_init', 'my_theme_sidebars_in' );

?>
<?php // post-thumbnails - image_size
add_theme_support( 'post-thumbnails');
// add_theme_support( 'post-thumbnails', array( 'post', 'slide', 'page') );
add_image_size( 'blog-thumb', 230, 180, true );
add_image_size( 'slider-img', 1000, 471, true ); // 775 * 365 
add_image_size( 'main-img', 420, 278, true ); // 370 x 245  
add_image_size( 'medium-img', 220, 220, true );
// add_image_size( 'widget-thumb', 80, 80, true );


function salas_remove_image_sizes($sizes) {
    unset($sizes['large'], $sizes['medium_large']);  /// 'medium' 
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'salas_remove_image_sizes');



if (class_exists('MultiPostThumbnails')) {
new MultiPostThumbnails(array(
'label' => 'Featured 2 Image',
'id' => 'feat-image',
'post_type' => 'post'
 ) );
}

?>
<?php 


function my_custom_types_in() {
	
register_post_type( 'fishki_2',
		array(
			'labels' => array(
				'name' => __( 'Fishki beate' ),
				'menu_name' => __( 'Fishki' ),
				'singular_name' => __( 'Fishka' ),
				'add_new' => __( 'Add New' ),
				'add_new_item' => __( 'Add New Fishka 1111' ),
				'edit_item' => __( 'Edit Fishka 1111' ),
			),			
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'comments' ),
			'public' => true,
			'show_in_menu' 	=> true,
			'menu_position' 	=> 5, 	
			'show_in_nav_menus' 	=> false,		
			'taxonomies' 			=> array( 'fishki_cats' ),
			/// 'rewrite' => array('slug' => 'product74', 'with_front' => false),
			'rewrite' 			=> true,
			// 'has_archive'			=> true,
			// 'exclude_from_search' 	=> true,	// !!!!	може викликати проблеми з показом категорій				
			// 'hierarchical' 			=> false,
			// 'menu_icon' 			=> $media_url4 . "icones/menu_icons42.png",
			// 'show_in_menu' 			=> 'edit.php?post_type=' . $post_type_7,
			// 'publicly_queryable' => false, // НЕ показувати на сайті (тільки в адмінці)		
		)
);

register_taxonomy('fishki_cats', array('fishki_2'), array(
			'labels' => array(
				'name' => __('Fishki categories'),
				'singular_name' => __('Fishki cat'),
				'menu_name' => __('Categories'),
				'edit_item' => __('Edit Fishki cat'),		
			),			
			'public' => true,	
			'hierarchical' => true,		
			'show_in_nav_menus' => true,
			'rewrite' => array('slug' => 'fishki_7', 'with_front' => false, 'hierarchical' => true),
			// 'rewrite' 			=> true,
));

}

//add_action( 'init', 'my_custom_types_in', 2 );








// підключити набір css, яваскриптів 
function add_my_scripts4() {

wp_enqueue_style( 'tema-wp-style', get_stylesheet_uri(), array(), '1.4', NULL ); /* style.css - Осн. файл стилів */
wp_enqueue_style( 'simple-icons', get_template_directory_uri().'/presentation/simple-icons.css', array(), NULL );

wp_register_script( 'prototype4', get_template_directory_uri().'/scripts/prototype.js#async', array(), NULL, false );
wp_enqueue_script( 'prototype4' );

if(!is_page()) {
 wp_enqueue_script( 'jquery' );
} else {
/*wp_deregister_script( 'jquery' );  wp_deregister_script( 'jquery-migrate' );
 wp_register_script( 'jquery', includes_url('/js/jquery/jquery.js#async'), array(), NULL, false );
 wp_enqueue_script( 'jquery' );
 wp_register_script( 'jquery-migrate', includes_url('/js/jquery/jquery-migrate.min.js#async'), array(), NULL, false );
 wp_enqueue_script( 'jquery-migrate' );*/
}
/*  */
wp_register_script( 'j_carousel', get_template_directory_uri().'/scripts/j_carousel.js', array(), '9.0', true );
wp_enqueue_script( 'j_carousel' );

wp_register_script( 'jquery_cycle4', get_template_directory_uri().'/scripts/jquery.cycle.js', array(), '1.0', true );
wp_enqueue_script( 'jquery_cycle4' );

// підключити jquery-ui з ядра 
// wp_enqueue_script('jquery-ui-core');
wp_enqueue_script('jquery-ui-slider');

wp_register_script( 'easyTooltip', get_template_directory_uri().'/scripts/easyTooltip.js', array(), '9.0', true );
wp_enqueue_script( 'easyTooltip' );

if(is_single()) {
wp_register_script( 'j_rating', get_template_directory_uri().'/scripts/jquery.rating.js', array(), '1.0', true );
wp_enqueue_script( 'j_rating' );
	
wp_register_script( 'cloud_zoom', get_template_directory_uri().'/scripts/cloud-zoom.js', array(), '9.0', true );
wp_enqueue_script( 'cloud_zoom' );
} // if(is_single()) 
/* 
wp_register_script( 'scroll_liver', get_template_directory_uri().'/scripts/scroll-liver.js', array(), '1.0', true );
wp_enqueue_script( 'scroll_liver' );
 */
}

add_action( 'wp_enqueue_scripts', 'add_my_scripts4' );

function add_async_forscript($url) {
if ( !is_admin() and (strpos($url, '#async')!==false) ) {
	$url = str_replace('#async', '', $url)."' async='async"; 
}
	return $url;
}
add_filter('clean_url', 'add_async_forscript', 11, 1);





function samorano_short_content($content, $cutti_num) {
// $short_content_2 = preg_replace('`\[[^\]]*\]`', '', strip_tags($content)); //
    $short_content_2 = strip_shortcodes( strip_tags($content) ); // WP function "strip_shortcodes"
    $short_content = $short_content_2;
    if( strlen($short_content_2) > $cutti_num ) {
        $charset = get_bloginfo('charset'); // $charset = 'UTF-8';
        $short_content = mb_substr($short_content_2, 0, $cutti_num, $charset);
        $short_content = substr($short_content, 0, strrpos($short_content, " ", 0)); // mb_strripos($short_content, ' ', 0, $charset)
    }
    return $short_content;
}





function add_admin_pages_columns_4() {
$p_types = array('page');
/// add_filter('manage_product_posts_columns', 'product_columns_head', 0);
foreach ($p_types as $p_type ) :  
 add_filter('manage_'.$p_type.'_posts_columns', 'page4_columns_head', 0);  
 add_action('manage_'.$p_type.'_posts_custom_column', 'page4_columns_content', 10, 2);
endforeach;
}
add_action('admin_init', 'add_admin_pages_columns_4');

function page4_columns_head($columns) { 
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'page_url' => __( 'Url' ),
		'date' => __( 'Date' )
	);    
	return $columns;  
}
function page4_columns_content($column_name, $post_ID) {  
	if ($column_name == 'page_url') {
		$post2 = get_post($post_ID);  $p_name = $post2->post_name;
		echo '<strong>'.$p_name.'</strong>';	
	}	
}  



 /* Адмінка. Сторінки - показати excerpt */ 
add_action( 'init', 'excerpts_to_pages' );
function excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}



/* **** Пошук. Включити в рез-ти лише окремі post_types **** */
function GetSearchfTypes($query) {
    if ($query->is_search) {
        $args4 = array( 'public' => true, '_builtin' => false ); 
		$types_arr = get_post_types($args4);  $types_arr['post'] = 'post';
		unset($types_arr['wow_order'], $types_arr['c_form_order'], $types_arr['writer-2']); // $types_arr['writer']
		$query->set('post_type', $types_arr);
    }
return $query;
}
add_filter('pre_get_posts','GetSearchfTypes');


function samorano_search_by_title( $search, $wp_query ) { // only title in search 
    if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
        global $wpdb;
        $q = $wp_query->query_vars;
        $n = ! empty( $q['exact'] ) ? '' : '%';
        $search = array();
        foreach ( ( array ) $q['search_terms'] as $term ) {
     $search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );
		}
        if ( ! is_user_logged_in() ) { $search[] = "$wpdb->posts.post_password = ''"; }
        $search = ' AND ' . implode( ' AND ', $search );
    }
    return $search;
}
add_filter( 'posts_search', 'samorano_search_by_title', 10, 2 );


?>
<?php // posts_in_category
function posts_in_category() {
	global $post;	
$term_ids = array();
if(is_archive()) : 
$queried_object = $wp_query->queried_object; 
$term_id = $queried_object->term_id;
$taxonomy = $queried_object->taxonomy;
if ($term_id) { $term_ids[0] = $term_id; }
// $curr_id = $term_id;
$curr_post_ids = array(); 
elseif(is_single()) : 
  	$post_type = get_post_type();   
  		$taxonomy_names = get_object_taxonomies($post);  $taxonomy = $taxonomy_names[0];
  		$terms = wp_get_post_terms($post->ID, $taxonomy);
if ($terms) {  foreach($terms as $ind_term) { $term_ids[] = $ind_term->term_id; }  }
  		// $term_4 = $terms[0];  $curr_id = $term_4->term_id;
		$curr_post_ids = array($post->ID);
endif;

 if(count($term_ids)) :  
$posts_args_2 = array (       
        'post_type'  => 'any',
		'posts_per_page'  => 5,
		// 'order' => 'DESC',	
		// 'orderby' => 'date',
		'post__not_in' => $curr_post_ids,	
		'tax_query' => array(
			array (
			'taxonomy' => $taxonomy, // 'category'
			// 'field' => 'term_id', // 'slug'
			'terms' => $term_ids // 'my-slug2'
			)
		), 
		'post_status' => 'publish'
    );

$query_25 = new WP_Query($posts_args_2);

    if( $query_25->have_posts() ) { ?>
<div class="post_add posts_in_category">
<h4><?php _e('Posts in category') ?>:</h4>
<ul>
<?php while ($query_25->have_posts()) {  $query_25->the_post(); ?>
<li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>
<?php } ?>
</ul>
</div>
<?php }  wp_reset_query(); 
endif; // if(count($term_ids))
}
?>
<?php 


// Posts_related
function posts_related() {
	global $post;
$tags = wp_get_post_tags($post->ID);
if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) { $tag_ids[] = $individual_tag->term_id; }
    $posts_args_3 = array (
        'post_type'  => 'any',
		'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'posts_per_page' => 5 // 
    );
    $my_query_2 = new WP_Query($posts_args_3);
    if( $my_query_2->have_posts() ) { ?>
    <div class="post_add posts_related">
    <h4><?php _e('Related posts') ?>:</h4>
        <ul>
        <?php while ($my_query_2->have_posts()) { $my_query_2->the_post(); ?>
 <li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>
        <?php } ?>
        </ul>
        </div>
<?php } wp_reset_query(); 
} // if ($tags) 
} 
?>
<?php 

/// пагінація 
function wp_corenavi($query_2) {
  global $wp_query, $wp_rewrite;
  if($query_2) { $navi_query = $query_2; } else { $navi_query = $wp_query; }
  $pages = '';
  $max = $navi_query->max_num_pages;
  if (!$current = get_query_var('paged')) { $current = 1; }
  $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
  $a['total'] = $max;
  $a['current'] = $current;

  $total = 1; // текст "Page 1 of 1", 1 - є, 0 - нема
  $a['mid_size'] = 3; // к-сть кнопок зліва і справа від активної кнопки
  $a['end_size'] = 1; // к-сть кнопок на початку і в кінці 
  $a['prev_text'] = '&laquo;'; // текст кнопки "Попередня стор."
  $a['next_text'] = '&raquo;'; //

  if ($max > 1) { 
  echo '<div class="navigation">';
  $pages = ''; if ($total == 1) { $pages = '<div class="pages">'. __('Page'). ' ' . $current . ' '.__('of').' ' . $max . '</div>'."\r\n"; }
  echo $pages . '<div class="pagi" id="pagi">' . paginate_links($a) . '</div>';
  echo '</div>';
  }
}

// /wp-includes/general-template.php 
// function paginate_links - після рядків $link = add_query_arg( $add_args, $link ); 
// у 2-х місцях (1 - <a class="prev page-numbers", 2 - <a class='page-numbers') вставити відповідно рядки:
// if($current == 2) { $link = str_replace('page/1/', '', $link); }
// if($n == 1) { $link = str_replace('page/1/', '', $link); } 

?>
<?php 
/* К-сть переглядів */ 
/* 
function count_page_hits() {
	 if(is_single()) {
        global $post;
      $count = get_post_meta($post->ID, 'count_page_hits', true);
      $count = $count + 1;
      update_post_meta($post->ID, 'count_page_hits', $count);
   } 
}
add_action('wp_head', 'count_page_hits');
 */

?>
<?php 
// breadcrumbs
function breadcrumbs() {
  // $showOnHome = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
  $delimiter = '<span class="zakarlu"><i class="fa fa-chevron-right"></i></span>'; // разделить 
  // $home = bloginfo('name'); // текст ссылка "Главная"
  $showCurrent = 1; // 1 - показывать название текущей статьи/страницы, 0 - не показывать
  $before = '<span class="current">'; // тег перед текущей "крошкой"
  $after = '</span>'; // тег после текущей "крошки"

  global $post; global $wp_query;
  // $homeLink = get_settings('siteurl');
    echo '<div class="title_content" id="crumbs"><a href="'; bloginfo('url'); echo '">'; 
	// bloginfo('name'); 
	_e('Home');
	echo '</a>' . $delimiter . ' ';
    
	if ( is_tax() or is_category() ) {  // is_category()
	$queried_object = $wp_query->queried_object;
	$taxonomy = $queried_object->taxonomy;
	$term4 = $queried_object;
	// $term4 = get_term($queried_object->term_id, $taxonomy);
	$term4_arr = array();
			while ($term4->parent) {
		$term4 = get_term($term4->parent, $taxonomy);
		$term4_arr[] = $term4;
			} // end while
		krsort($term4_arr);
	foreach ($term4_arr as $key4 => $term) :
		echo '<a href="'; echo get_term_link( $term ) . '">' . $term->name . '</a>'; echo $delimiter;
	endforeach;
	  if ($showCurrent == 1) echo $before . $queried_object->name . $after;
	}
	
	elseif ( is_single() && !is_attachment() ) {       
		$main_post_id = $post->ID;
	if($post->post_parent) { ////
		$parent4_arr = array();
		$parent4 = $post->post_parent;
			while ($parent4) {
			$parent4_arr[] = $parent4;
			$post_4 = get_post($parent4);
			$parent4  = $post_4->post_parent;			
			} // end while
		krsort($parent4_arr);  $parent4_arr = array_values($parent4_arr);
		$main_post_id = $parent4_arr[0]; // highest parent post
	} // if($post->post_parent)
		$taxonomy_names = get_object_taxonomies( $post );  $taxonomy = $taxonomy_names[0];
		$terms = wp_get_post_terms( $main_post_id, $taxonomy );  $term4 = $terms[0];		
		if($term4) { ///
		$term4_arr = array();
		$term4_arr[] = $term4;
			while ($term4->parent) {	
		$term4 = get_term($term4->parent, $taxonomy);
		$term4_arr[] = $term4;
			} // end while
		krsort($term4_arr);
		foreach ($term4_arr as $key4 => $term) :
		echo '<a href="'; echo get_term_link( $term ) . '">' . $term->name . '</a>'; echo $delimiter;
		endforeach;
		} // if($term4)
	if($post->post_parent) { 
		foreach ($parent4_arr as $key2 => $post_id) : 
		echo '<a href="' . get_permalink($post_id) . '">' . get_the_title($post_id) . '</a>' . $delimiter;
		endforeach;
	} // if($post->post_parent)
	  if ($showCurrent == 1) echo $before . get_the_title() . $after; 
    } 


	elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
    } 
	elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
    } 
	
	elseif ( is_search() ) {
      echo $before . '"' . get_search_query() . '"' . $after;
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
    } 	
	 
	elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      /*  $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;  */
    } 
	
	elseif ( is_tag() ) {
      echo $before . '"' . single_tag_title('', false) . '"' . $after;
    } elseif ( is_author() ) {
      global $author;
      $userdata = get_userdata($author);
      echo $before . '' . $userdata->display_name . $after;
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
/* 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' <span class="cur_page">(';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')</span>';
    }
 */
    echo '</div>';
}
 // end breadcrumbs()
 
 
?>
<?php
// Зачистити HEAD від зайвого 
    remove_action('wp_head', 'rsd_link');
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'parent_post_rel_link'); // prev link    
	remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'start_post_rel_link', 10, 0 );
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	

 function remove_footer_admin_1 () { /*  */ } 
 add_filter('admin_footer_text', 'remove_footer_admin_1');
 
 function admin_footer_version() {	return ''; }
 add_filter( 'update_footer', 'admin_footer_version', 12);
 
 


/* *** disable XML-RPC in WordPress (pingbacks, ...) */
add_filter( 'xmlrpc_enabled', '__return_false' );  // stop all remote requests using XML-RPC
add_filter( 'wp_headers', 'samorano_remove_x_pingback' );  // hide xmlrpc.php in HTTP response headers
function samorano_remove_x_pingback( $headers ) {  
    unset( $headers['X-Pingback'] );
    return $headers;
}


 
/* 
function remove_menus_5() { // //// зачистка меню адмінки 
 global $menu;
	//  $restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'), __('Slides')); end ($menu); 
	// while (prev($menu)){$value = explode(' ',$menu[key($menu)][0]);if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}}
 
/// повністю прибрати меню адмінки  
global $menu;   
 $menu = array();  // Очищаем массив с пунктами меню 
 echo "<style type='text/css'>#adminmenuwrap, #adminmenuback{display:none!important;} #wpcontent{margin-left:10px!important;}</style>"; // скрываем панль
}

if( !current_user_can('administrator') and !current_user_can('editor') ) {
	add_action('admin_menu', 'remove_menus_5');
}
 */


 /* прибрати адмін бар з сайту (але не з адмінки) */
 add_filter('show_admin_bar', '__return_false');



/* Адмінка. Редагування публікації. Дерево категорій - зберегти норм. ієрархію */
add_filter( 'wp_terms_checklist_args', 'admin_cats_checklist_args' );
function admin_cats_checklist_args( $args ) {
		$args['checked_ontop'] = false;
		return $args;
}

// Адмінка. Галерея. Налаштування за замовчуванням 
function my_gallery_default_type_set_link( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
	$settings['galleryDefaults']['columns'] = 4;
    return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');

 
?>
<?php



/* Додати новий пункт налаштувань в адмінці у розділ Settings (сторінка - reading) */
require_once ( TEMPLATEPATH . '/lib/admin_new_settings_4.php' );



add_action('add_meta_boxes', 'add_besto_post_boxes');
function add_besto_post_boxes() {
$p_types = array('post'); // 'post', 'page' ....
foreach ($p_types as $p_type ) :  
 // add_meta_box('in_main_slider', __('Show in slider'), 'set_in_main_slider', $p_type, 'side', 'low');
 add_meta_box('in_bottom_slider', __('Featured Article'), 'set_featured_article', $p_type, 'side', 'low');
 remove_meta_box('postcustom', $p_type, 'normal');
endforeach;
}

// function set_in_main_slider() { add_new_meta_checkboxi('show_in_main_slider'); }
function set_featured_article() { add_new_meta_checkboxi('featured_article'); }

function add_new_meta_checkboxi($meta_key_1) {
	global $wpdb;
	global $post;
	// $meta_key_1 = 'show_in_main_slider';
	add_post_meta($post->ID, $meta_key_1, '', true);
	$post_meta_arr_1 = $wpdb->get_row( "SELECT * FROM $wpdb->postmeta WHERE post_id = $post->ID AND meta_key = '$meta_key_1'", ARRAY_A );
	$meta_input_id = $post_meta_arr_1['meta_id']; 
	$input_value = $post_meta_arr_1['meta_value'];
	
	$item_input_name = 'meta['.$meta_input_id.'][value]';
	$item_input_key = 'meta['.$meta_input_id.'][key]';

echo '<input type="hidden" name="'.$item_input_key.'" value="'.$meta_key_1.'" />';
echo '<input type="hidden" name="'.$item_input_name.'" value="0" />'; ///
echo '<input type="checkbox" name="'.$item_input_name.'" id="bo_'.$meta_key_1.'" value="1"';
if ($input_value == 1) { echo ' checked="checked"'; }
echo '/>';
echo '<label for="bo_'.$meta_key_1.'"> '.__('Enable').' </label>';
// echo '___ '.$input_value; /// 
}








define( 'WOW_DIRE', TEMPLATEPATH . '/lib/wow_e_shop/' ); 
require_once ( WOW_DIRE . 'wow_e_shop.php' );

/* // 1 simple site
include TEMPLATEPATH . '/lib/wow_functions_2/z_wow_product_list.php';
include TEMPLATEPATH . '/lib/wow_functions_2/z_wow_form_order.php';
include TEMPLATEPATH . '/lib/wow_functions_2/z_wow_cat_images.php';
 */

 
 /* Multi Post Thumbnails */
 
require_once ( TEMPLATEPATH . '/lib/multi_post_thumbnails.php' );
/// front code - single-post.php 
if (class_exists('MultiPostThumbnails')) {
$p_types = array('page');
foreach($p_types as $p_type) {
	new MultiPostThumbnails( array(
'label' => 'Featured 2 Image',
'id' => 'thumbnail-feat',
'post_type' => $p_type 
	) );
}
}

// Заборонити оновлення файлів локалізації
add_filter( 'auto_update_translation', '__return_false' );



// IE fix. Розкоментувати, щоб включити костиль для IE
/*
	wp_register_script( 'celta_modernizr', get_bloginfo('template_directory') . '/scripts/modernizr.js', false, '1.0' );
	wp_enqueue_script( 'celta_modernizr' );
 */ 	


// Розкоментувати, щоб включити слайдер і функцію вставки табів, виїжджаючих блоків і т.д. через адмінку
/*
   define( 'VASYA', TEMPLATEPATH . '/lib/' );
require_once ( VASYA . 'theme-options.php' );
// !! script celta_cycle 
 */


// Розкоментувати, щоб включити скрипт для адаптивної верстки
/* 
wp_register_script( 'res_modernizr', get_template_directory_uri().'/scripts/js_responsive/responsive-modernizr.min.js', false, '1.0' );
wp_enqueue_script( 'res_modernizr' );
 */

//Добавление опции 1
add_action('admin_init' ,'main_funk');
function main_funk(){
	// register_setting('general', 'news_option ');
	// add_settings_field('id', 'background header', 'func', 'general');

	// register_setting('general', 'news_option2 ');
	// add_settings_field('idi', 'btn: see a doctor now', 'func2', 'general');

	register_setting('general', 'news_option3 ');
	add_settings_field('idin', 'Link: Login', 'func3', 'general');
}
function func(){
?>
<input class="custom_media_url" type="text" name="news_option" value="<?php echo get_option('news_option'); ?>">  
<?
}

function func2(){
?>
<input class="custom_media_url" type="text" name="news_option2" value="<?php echo get_option('news_option2'); ?>">  
<?
}

function func3(){
?>
<input class="custom_media_url" type="text" name="news_option3" value="<?php echo get_option('news_option3'); ?>">  
<?
}


// *********
 
function select_attr($a){
  $cod_position = get_post_custom( get_the_ID() ); 
  $cod_position = $cod_position[$a];
  global $wpdb;
  foreach ($cod_position as $value_in) {
   $res_last_posts = $wpdb->get_results("SELECT label FROM wp_wow__attribute_value_options WHERE id=$value_in"); 
   if(!empty($res_last_posts[0]) ){ 
         $a_array = $res_last_posts[0];
        echo  "<span class='br'>".$a_array->label."</span>"; 
   }
  }
}



//**************
add_action('add_meta_boxes', 'add_author_custom_box');
function add_author_custom_box() {
 //$types = array('post', 'consultant','writer');
 $types = array('post');
 foreach ($types as $type) {
        $post_type = $type;
add_meta_box('post_author_custom', __('Consultant, writer'), 'post_author_custom_f', $post_type, 'normal', 'low');
 }
}

function post_author_custom_f() {
                global $wpdb;
                global $post;                
        $meta_key_1 = 'post_author_custom';
        add_post_meta($post->ID, $meta_key_1, '', true);
        $post_meta_arr_1 = $wpdb->get_row( "SELECT * FROM $wpdb->postmeta WHERE post_id = $post->ID AND meta_key = '$meta_key_1'", ARRAY_A );
        $meta_input_id = $post_meta_arr_1['meta_id']; 
        $input_value = $post_meta_arr_1['meta_value'];
                $p_author = $input_value; 
        $item_input_name = 'meta['.$meta_input_id.'][value]';
        $item_input_key = 'meta['.$meta_input_id.'][key]';
        
        echo '<input type="hidden" name="'.$item_input_key.'" value="'.$meta_key_1.'" />'; ///

$author_types = array (       
 'consultant' => array('order' => 'ASC', 'orderby' => 'title', 'title_1' => __('Select consultant')),
 'writer' => array('order' => 'ASC', 'orderby' => 'title', 'title_1' => __('Select simple author')),
);
        foreach ($author_types as $p_type => $author_arr) :  /////
$posts_args_4 = array (       
        'post_type'  => $p_type,
                'order' => $author_arr['order'],        
                'orderby' => $author_arr['orderby'], // 'menu_order' 
                'posts_per_page' => -1,
                'post_status' => 'publish',
);
$author_4_query = new WP_Query($posts_args_4);
if( $author_4_query->have_posts() ) {
        echo '<div class="box_1">';
        echo '<div class="tit"><strong>'.$author_arr['title_1'].'</strong></div>';
                echo '<span class="line_lab_2">';
                echo '<label class="line_lab" for="nothing_87_'.$p_type.'">---'.__('No author').'---</label>  ';
                echo '<input type="radio" id="nothing_87_'.$p_type.'" name="'.$item_input_name.'" value="" />';
                echo '</span>';        
        while ($author_4_query->have_posts()) :  $author_4_query->the_post(); ///
        $post_id = $post->ID;
        $author_id = 'author-hx-'.$post_id;
        $check = ''; if($p_author == $post_id) { $check = ' checked="checked"'; }
        echo '<span class="line_lab_2">';
        echo '<label class="line_lab" for="'.$author_id.'">'.get_the_title().'</label>  ';
        echo '<input type="radio" id="'.$author_id.'" name="'.$item_input_name.'" value="'.$post_id.'"'.$check.' />';
        echo '</span>';        
        endwhile; ///
        echo '</br></br> </div>';
}  wp_reset_query(); // if( $author_4_query->have_posts() )
        endforeach; /////
}


/*  widget  */
add_action( 'widgets_init', 'funk_widget' );
function funk_widget(){
	register_widget( 'New_Img_Widget' );
}
class New_Img_Widget extends WP_Widget{
function __construct(){
$args = array(
'name' => 'New images widget',
'description' => 'New images widget',
'classname' => 'class-main');
parent::__construct('id_in', '', $args);
} 
function widget($args, $instance){ 
//var_dump($args); echo "<br>----"; var_dump($instance);
echo '<div class="widget-image">';
extract($args); 
extract($instance); 
?>
<?php if(!empty($title)) { ?>
<h4><?php echo "$title"; ?></h4>
<?php } ?>
<?php if(!empty($text)) { ?>
<span><?php echo "$text"; ?></span>
<?php } ?>

<ul>
<?php if(!empty($url1) || !empty($img1)) { ?>
	<li><a href="<?php echo $url1 ?>"><img src="<?php echo $img1 ?>" alt=""></a></li>
<?php } ?>

<?php if(!empty($url2) || !empty($img2)) { ?>
	<li><a href="<?php echo $url2 ?>"><img src="<?php echo $img2 ?>" alt=""></a></li>
<?php } ?>

<?php if(!empty($url3) || !empty($img3)) { ?>
	<li><a href="<?php echo $url3 ?>"><img src="<?php echo $img3 ?>" alt=""></a></li>
<?php } ?>
</ul>

<?php
echo "</div>";
}


function form( $instance){
	extract($instance);
?>
<script>
jQuery(document).ready(function($) {
	$('.add_img_in').click(function(event) {
		var this_in = $(this);
		var send_attachment_bkp = wp.media.editor.send.attachment;
		wp.media.editor.send.attachment = function(props, attachment) {
			this_in.addClass('add_img_in_action');
			$('.add_img_in_action').prev('img').attr('src', attachment.url);
			$('.add_img_in_action').prev('img').prev('input').attr('value', attachment.url);
			$('.add_img_in_action').prev('img').css('display', 'block');
			$('.add_img_in_action').css('display', 'none');
			$('.add_img_in_action').next('a').css('display', 'block');
			$('.add_img_in_action').removeClass('add_img_in_action');
			wp.media.editor.send.attachment = send_attachment_bkp;
		}
		wp.media.editor.open();
		return false;
	});
	$('.delete_img').click(function(event) {
		$(this).prev('a').css('display', 'block');
		$(this).css('display', 'none');
		$(this).prev('a').prev('img').css('display', 'none').attr('src', '');
		$(this).prev('a').prev('img').prev('input').attr('value', '');
		return false;
	});
});
</script>
<p>
<label for="<?php echo $this->get_field_id('title') ?>">Headline:</label>
<input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" value="<?php if( isset($title) ) echo esc_attr( $title ); ?>" class="widefat">
</p>
<p style="border-bottom: 1px solid #ccc; padding-bottom: 15px;">
<label for="<?php echo $this->get_field_id('text'); ?>">Text:</label>
<textarea name="<?php echo $this->get_field_name('text') ?>" id="<?php echo $this->get_field_id('text') ?>" value="<?php if( isset($text) ) echo esc_attr( $text ); ?>" class="widefat" rows="5" cols="20"><?php if( isset($text) ) echo esc_attr( $text ); ?></textarea>
</p>

<p style="border-bottom: 1px solid #ccc; padding-bottom: 15px;">
 <input style="margin-bottom: 11px;" placeholder="url" type="text" name="<?php echo $this->get_field_name('url1') ?>" value="<?php  echo $url1; ?>">
 <input type="hidden" name="<?php echo $this->get_field_name('img1') ?>" value="<?php  echo $img1; ?>">
 <?php if(empty($img1)){ $dis = 'display:none;';  } else{$dis = 'display:block;';} ?>
  <img  style="<?php echo "$dis"; ?> max-width: 120px; max-height: 130px;" class="img_widgets_in" src="<?php echo $img1; ?>" alt="">
<?php
	if(empty($img1)){
	echo '<a style="display:block;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:none;" class="delete_img" href="#">delete_img</a>';
	}else{
	echo '<a style="display:none;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:block;" class="delete_img" href="#">delete_img</a>';
	}
?>
</p>
<p style="border-bottom: 1px solid #ccc; padding-bottom: 15px;"> 
 <input style="margin-bottom: 11px;" placeholder="url" type="text" name="<?php echo $this->get_field_name('url2') ?>" value="<?php  echo $url2; ?>">
 <input type="hidden" name="<?php echo $this->get_field_name('img2') ?>" value="<?php  echo $img2; ?>">
 <?php if(empty($img2)){ $dis2 = 'display:none;';  } else{$dis2 = 'display:block;';}?>
  <img  style="<?php echo "$dis2"; ?> max-width: 120px; max-height: 130px;" class="img_widgets_in" src="<?php echo $img2; ?>" alt="">
<?php
	if(empty($img2)){
	echo '<a style="display:block;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:none;" class="delete_img" href="#">delete_img</a>';
	}else{
	echo '<a style="display:none;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:block;" class="delete_img" href="#">delete_img</a>';
	}
?>
</p>
<p style="border-bottom: 1px solid #ccc; padding-bottom: 15px;"> 
 <input style="margin-bottom: 11px;" placeholder="url" type="text" name="<?php echo $this->get_field_name('url3') ?>" value="<?php  echo $url3; ?>">
 <input type="hidden" name="<?php echo $this->get_field_name('img3') ?>" value="<?php  echo $img3; ?>">
 <?php if(empty($img3)){ $dis3 = 'display:none;';  } else{$dis3 = 'display:block;';} ?>
  <img  style="<?php echo "$dis3"; ?> max-width: 120px; max-height: 130px;" class="img_widgets_in" src="<?php echo $img3; ?>" alt="">
<?php
	if(empty($img3)){
	echo '<a style="display:block;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:none;" class="delete_img" href="#">delete_img</a>';
	}else{
	echo '<a style="display:none;" class="add_img_in" href="#">add_img</a>';
	echo '<a style="display:block;" class="delete_img" href="#">delete_img</a>';
	}
?>
</p>
<?php
} 
}
/* end  widget  */


//function remove_cssjs_ver( $src ) {
//if( strpos( $src, '?ver=' ) )
//$src = remove_query_arg( 'ver', $src );
//return $src;
//}
//add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
//add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );