<?php
/*
Template Name: Homepage Template
*/
?>

<?php get_header(); ?>


<div class="home_page no_column blog">


<?php // echo get_option('news_option'); ?>

   
<div class="content">




<div class="wrap-page-home">
   <?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>   
<h1><?php the_title(); ?></h1><br />
<?php the_content(); ?>    
	<?php // -//- end main content ?> <?php endwhile; ?>	<?php else : ?>  	<?php endif; ?>	  
</div>



<?php 
	$fishki_args = array (
		'post_type'   => 'consultant', // 'fishki89', 'post';  'any' - усі типи 
		'posts_per_page' => 30, // -1
		'order' => 'ASC',	
		'orderby' => 'menu_order', // 'title', 'date', 'modified', 'comment_count', 'menu_order' /// 'meta_value_num' 
		/// 'meta_key' => 'views', // if use 'orderby' => 'meta_value_num' 
		'post_status' => 'publish'
    );
    $my_query_3 = new WP_Query($fishki_args); 
if( $my_query_3->have_posts() ) : ?>
<div class="box-content co_home_consultant"> 
<?php /* <div class="tit"> <h3><?php _e('Recent posts') ?></h3> </div> */ ?>
<?php $slider_name = 'home_consultant'; ?>
<?php /* jquery  */ ?> <?php /* script jCarousel */ ?>
<?php /* * jQuery завантажується асинхронно. Якщо jQuery ще не завантажився, вставляємо його додатково * */ ?>    
	<div class="hslider-container">   
<script type="text/javascript">
jQuery(document).ready(function($) {
        var slides_count = <?php echo $my_query_3->post_count; ?>;  var count_42 = 1;
		var jcarousel = $('.<?php echo $slider_name; ?>.horizontal-slider');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                /// if (width >= 750) { width = width / 4; count_42 = slides_count - 4; } 
				if (width >= 900) { width = width / 3; count_42 = slides_count - 3; }
                else if (width >= 595) { width = width / 2; count_42 = slides_count - 2; }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
				if(count_42 <= 0) { jcarousel.parent().addClass("no_slide_navi"); }
            })
            .jcarousel({ wrap: 'circular' });

        $('.hslider-prev.<?php echo $slider_name; ?>').jcarouselControl({ target: '-=1' });
        $('.hslider-next.<?php echo $slider_name; ?>').jcarouselControl({ target: '+=1' });

        $('.controls.<?php echo $slider_name; ?>')
            .on('jcarouselpagination:active', 'a', function() { $(this).addClass('activeSlide'); })
            .on('jcarouselpagination:inactive', 'a', function() { $(this).removeClass('activeSlide'); })
            .on('click', function(e) { e.preventDefault(); })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) { return '<a href="#' + page + '">' + page + '</a>'; }
            });
});
</script>  
            
   		<div class="<?php echo $slider_name; ?> horizontal-slider">		
            <ul>
				<?php while ($my_query_3->have_posts()) : 
				$my_query_3->the_post(); 		
				// global $more;  $more = 0;  // необхідно для тегу <!--more--> ?>
					<li class="item">
                    <div class="slider_lift">                        
           <div class="prod-image">
	<a class="product-image" href="<?php the_permalink(); ?>" ><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'medium-img' ); } else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/ntrmds_icons/client.svg" class="no_feat" /> </div>'; } ?></a>						
           </div>
	<h5 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

<div class="bl-info-in">
<?php $post_id = $post->ID; 
  include WOW_DIRE.'front_html_blocks/attributes.php'; /* wow_e_shop *** attributes *** */ 
?>
<div class="bl-info">
	<span class="bl"><a class="profil" href="<?php the_permalink(); ?>" ><img src="<?php echo get_bloginfo('template_url').'/images/ntrmds_icons/client.svg'; ?>"> <?php _e('Profile'); ?></a></span>
<?php // $prod_video = get_post_meta($post->ID, 'prod_video', true); 
$prod_video_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'prod_video'); ?>
    <?php if($prod_video_6['atr_value']) : 
	$prod_video = implode(', ', $prod_video_6['atr_value']); ?>
	<span class="bl"><a class="video various fancybox.iframe" href="<?php echo $prod_video; ?>"><img src="<?php echo get_bloginfo('template_url').'/images/ntrmds_icons/camera.svg'; ?>"> <?php echo $prod_video_6['frontend_label'] ?></a></span> 
	<?php endif; ?>
<?php $sessionfee_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'sessionfee'); ?>
    <?php if($sessionfee_6['atr_value']) : 
	$sessionfee = implode(', ', $sessionfee_6['atr_value']); ?>
	<span class="bl bl-fee"><?php echo $sessionfee_6['frontend_label'] ?><span>:</span> <?php echo $sessionfee ?> </span> 
	<?php endif; ?> 
</div>
</div>  
<?php // $schedule = get_post_meta($post->ID, 'schedule_session', true); 
$schedule_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'schedule_session'); ?>
    <?php if($schedule_6['atr_value']) : 
	$schedule = implode(', ', $schedule_6['atr_value']); ?>
 <a target="_blank" href="<?php echo $schedule ?>" class="btn-session"><?php echo $schedule_6['frontend_label'] ?></a> 
	<?php endif; ?>           
    				</div>
					</li>                    
				<?php endwhile; // ?>
			</ul>
		</div>   

    <?php if ($my_query_3->post_count > 1) : ?>
 <div class="hslider-nav hslider-prev <?php echo $slider_name; ?>"></div>
 <div class="hslider-nav hslider-next <?php echo $slider_name; ?>"></div>
            
            <div class="controls <?php echo $slider_name ?>"> </div>
    <?php endif; ?>       
	</div> 
   
</div>
<?php endif;  wp_reset_query(); ?> 
   




<?php
$options4 = get_option('site_add_settings_4');
$cat_ids_2 = $options4['home_health_cat_ids']; // What we treat
$cat_ids = explode(',', $cat_ids_2);
$taxonomy = 'health-cat';
if(count($cat_ids)) : ?>

<?php foreach($cat_ids as $cat_id) : ?>
<?php 
$term_id = (int) $cat_id;  if(term_exists($term_id, $taxonomy)) { // if tax_query
$term2 = get_term($cat_id, $taxonomy);
$cat_title = $term2->name;  $cat_slug = $term2->slug;  $cat_link = get_term_link($term2);
if(get_post_meta($post->ID, 'page_we_treat_title', true)) { $cat_title = get_post_meta($post->ID, 'page_we_treat_title', true); }
} // if tax_query 
$posts_args_2 = array (       
        'post_type'  => 'any', // 
		'posts_per_page' => 16,
		'order' => 'ASC',	
		'orderby' => 'title', // 'title', 'date',  'menu_order' /// 
		'tax_query' => array(
			array (
			'taxonomy' => $taxonomy, // 'category'
			'terms' => $cat_id 
			)
		),
		'post_status' => 'publish'
    );

$query_2 = new WP_Query($posts_args_2);

    if( $query_2->have_posts() ) { ?>
<div class="box-content maine main-what <?php echo $cat_slug ?>">
<div class="blog-top">
<div class="tit"> <h3><?php echo $cat_title ?></h3> </div>
<?php if($term2->description) { ?> 
<div class="descr"><?php echo term_description($term_id, $taxonomy) ?> </div>
<?php } ?>
</div>

<div class="grid_cont">
<ul class="products-grid cols_4 blog-list">
<?php	
	while ($query_2->have_posts()) : 
	$query_2->the_post(); 
		// global $more;  $more = 0;  // необхідно для тегу <!--more-->
?>  
 <li class="item">
 <div class="inn_cont">
<a href="<?php the_permalink(); ?>">
<h4> <?php the_title(); ?> </h4> 
<?php if ( has_post_thumbnail() ) { ?>
	<div class="thumbnail_5"> <?php the_post_thumbnail( ); ?> </div>				
<?php } ?> 			
</a>
 </div>
 </li> 
<?php endwhile; ?>
</ul>
</div>
</div>
<?php }  wp_reset_query(); ?>
<?php endforeach; ?>

<?php endif; // (count($cat_ids)) ?>
  
  
  
  
   


<?php /* * jQuery завантажується асинхронно. Якщо jQuery ще не завантажився, вставляємо його додатково * */ ?>
<?php /* 
<script type="text/javascript">
if (!window.jQuery) { document.write('<script src="<?php echo includes_url('/js/jquery/jquery.js'); ?>"><\/script>'); }
/// https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js 
// alert('no jQuery ...'); 
</script>
 */ ?>
 
<?php /* * Products sliders * */ ?>
<?php // include WOW_DIRE.'front_html_blocks/front_products_sliders.php'; /* wow_e_shop *** products_sliders *** */ ?> 

  



  
  


<?php
$options4 = get_option('site_add_settings_4');
$cat_ids_2 = $options4['home_blog_cat_ids']; // Blog
$cat_ids = explode(',', $cat_ids_2);
$taxonomy = 'category';
if(count($cat_ids)) : ?>

<?php foreach($cat_ids as $cat_id) : ?>
<?php 
$term_id = (int) $cat_id;  if(term_exists($term_id, $taxonomy)) { // if tax_query
$term2 = get_term($cat_id, $taxonomy);
$cat_title = $term2->name;  $cat_slug = $term2->slug;  $cat_link = get_term_link($term2);

if(get_post_meta($post->ID, 'latest_posts_title', true)) { $cat_title = get_post_meta($post->ID, 'latest_posts_title', true); }
if(get_post_meta($post->ID, 'page_blog_link', true)) { $cat_link = get_bloginfo('url').get_post_meta($post->ID, 'page_blog_link', true); }
$more_text_2 = __('Load More'); if(get_post_meta($post->ID, 'page_more_text_2', true)) { $more_text_2 = get_post_meta($post->ID, 'page_more_text_2', true); }
} // if tax_query 
$posts_args_2 = array (       
        'post_type'  => 'any', // 'fishki89', 'post';  'any' - усі типи  
		'posts_per_page'   => 6,
		// 'order' => 'DESC',	
		// 'orderby' => 'date', // 'title', 'date', 'modified', 'comment_count', 'menu_order' /// 'meta_value_num' 
		/// 'meta_key' => 'views', // if use 'orderby' => 'meta_value_num'
		'tax_query' => array(
			array (
			'taxonomy' => $taxonomy, // 'category'
			// 'field' => 'term_id', // 'slug'
			'terms' => $cat_id // 'my-slug2'
			)
		), 
		'post_status' => 'publish'
    );

$query_2 = new WP_Query($posts_args_2);

    if( $query_2->have_posts() ) { ?>
<div class="box-content maine home_blog <?php echo $cat_slug ?>">
<div class="blog-top">
<div class="tit"> <h3><?php echo $cat_title ?></h3> </div>
<?php if($term2->description) { ?> 
<div class="descr"><?php echo term_description($term_id, $taxonomy) ?> </div>
<?php } ?>
</div>

<div class="grid_cont">
<ul class="products-grid cols_3 blog-list">
<?php	
	while ($query_2->have_posts()) : 
	$query_2->the_post(); 
		// global $more;  $more = 0;  // необхідно для тегу <!--more-->
?>  
 <li class="item">
 <div class="inn_cont">
<?php if ( has_post_thumbnail() ) { ?>
	<div class="thumbnail_5"> <a href="<?php the_permalink(); ?>" ><?php the_post_thumbnail( 'main-img' ); ?></a> </div>				
<?php } ?> 			
<h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a> </h3> 
<div class="ty">
 <?php $avtor_id = get_post_meta($post->ID, 'post_author_custom', true);
  $avtor_p_type = get_post_type($avtor_id); 
   $avtor_name = get_the_title($avtor_id);  $avtor_link = get_the_permalink ($avtor_id); ?>
	<span class="author <?php echo $avtor_p_type; ?>"><em><?php _e('by') ?></em> <a href="<?php echo $avtor_link; ?>"><?php echo $avtor_name; ?></a></span>
	<span class="cat"><em><?php _e('in') ?></em><?php echo the_category(); ?></span>
</div>
<?php $cutti_num = 200;
// $short_content_2 = preg_replace('`\[[^\]]*\]`', '', strip_tags(get_the_content())); // 
$short_content_2 = strip_shortcodes( strip_tags(get_the_content()) ); // WP function "strip_shortcodes"
$charset = get_bloginfo('charset'); // $charset = 'UTF-8';
$short_content = mb_substr($short_content_2, 0, $cutti_num, $charset); 
$short_content = mb_substr($short_content, 0, strripos($short_content, ' ', 0, $charset), $charset);
?>
          <div class="descr entry-content"> <p><?php echo the_excerpt(); ?></p> </div>
 </div>
 </li> 
<?php endwhile; ?>
</ul>

<div class="more_line"> <a class="show-more" href="<?php echo $cat_link ?>" ><?php echo $more_text_2; ?><?php // _e('More...'); ?></a> </div>
	
<?php dynamic_sidebar( 'images_widget_in');  ?>

</div>

</div>
<?php }  wp_reset_query(); ?>
<?php endforeach; ?>

<?php endif; // (count($cat_ids)) ?>
	<div class="wrap-btn-in">
					<?php echo get_post_meta($post->ID, 'page_book_session', true); ?>
				</div>









</div> <!-- content -->



 
      
   
  
           
</div>      
	


 
     
     <?php /* 
	 Красивий чекбокс і радіо: class - fine_checkbox, fine_radio  

[:ua]Місто[:ru]Город[:en]City[:]
Старий:  <!--:ua-->Місто<!--:--><!--:en-->City<!--:-->
		 переклад для bloginfo('description'), widget_title :  [:ua]Місто[:en]City

if ( is_front_page() )
if ( is_single() )
if ( is_archive() )


Файли для окремої стор. і категорії типу "fishki" (taxonomy - "fishki-cat")
single-fishki.php 
taxonomy-fishki-cat.php

Проблема з активним пунктом меню, якщо для товарів включений параметр 'hierarchical' => true,
/wp-includes/nav-menu-template.php
знайти першу умову " ! is_post_type_hierarchical() " і вилучити цю частину умови
	 */ ?>



<?php /* Показати малюнки в пунктах меню
файл /wp-includes/ nav-menu-template.php

** перед рядком "$item_output = $args->before;" вставити код:
///// menu images /////
$item_img = '';
if($depth == 1) : /// set the depth: 0, 1, 2, ...
if($item->type == 'taxonomy') { 
$tax_7 = get_term_by('id', $item->object_id, $item->object); 
if($tax_7->term_thumbnail) { $item_img = wp_get_attachment_image( $tax_7->term_thumbnail, 'thumbnail' ); } 
} elseif($item->type == 'post_type') { 
if ( has_post_thumbnail($item->object_id) ) { $item_img = get_the_post_thumbnail( $item->object_id, 'thumbnail' ); } 
}
endif;

** блок "$item_output = $args->before; .... $item_output .= $args->after;" замінити на: 
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $item_img; ///// menu images /////
		$item_output .= $args->link_before . '<span>'.$title.'</span>' . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
 */ ?>





<!--  ****** -->



<?php get_footer(); ?>