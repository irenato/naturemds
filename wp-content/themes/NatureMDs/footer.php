</div>
</section>
<!-- END: main content -->

<?php 



?>
<footer id="site_footer">


<div class="foot_main">
<div class="wrapper-cont">
<div class="footer_inn">
<div class="box-sidebar box_1" id="futer_left"> <?php dynamic_sidebar( 'futer_left' ); ?> </div>
<div class="box-sidebar box_2" id="futer_centr"><?php /* 3 віджети = 3 колонки */ ?>
<?php dynamic_sidebar( 'futer_centr' ); ?> 
</div>
<div class="box-sidebar box_3 soc_icons" id="futer_right"> 
<?php dynamic_sidebar( 'futer_right' ); ?>
</div> 
<div class="footer-right-in">
	<?php dynamic_sidebar( 'futer_social' ); ?>
</div>
<div class="copyright"><?php dynamic_sidebar( 'futer_copyright' ); ?></div>
</div> <!-- footer_inn -->
</div>
</div>
<?php /* 
// Кнопки соц. мереж - код для вставки у textwidget 
// Зовн. блок повинен мати клас ".soc_icons"
<ul>
<li><a class="face" href="https://www.facebook.com/1111111" title="" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a class="vk" href="http://vk.com/1111" title="" target="_blank"><i class="fa fa-vk"></i></a></li>
<li><a class="tweet" href="https://twitter.com/1111" title="" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a class="google" href="https://plus.google.com/11111" title="" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a class="vimeo" href="https://vimeo.com/111111111" title="" target="_blank"><i class="fa fa-vimeo"></i></a></li>
<li><a class="youtub" href="http://www.youtube.com/channel/111111" title="" target="_blank"><i class="fa fa-youtube"></i></a></li>
<li><a class="tumblr" href="http://www.youtube.com/channel/111111" title="" target="_blank"><i class="fa fa-tumblr"></i></a></li>
<li><a class="inst" href="https://instagram.com/111111111" title="" target="_blank"><i class="fa fa-instagram"></i></a></li>
<li><a class="skype" href="skype:skype_login_1?chat" title=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
</ul>
 */ ?>
 
<?php /* Зразок тексту-списку у textwidget 
<h3>Наші переваги:</h3>
<ul>
<li> індивідуальний підхід до клієнта </li>
<li> оперативне виконання замовлень (3 робочі дні для сайту-візитки) </li>
<li> невисокі ціни </li>
<li> якість роботи, підтверджена нашим портфоліо </li>
</ul>
 */ ?>

<!-- <div class="foot_bot line">
<div class="wrapper-cont">
<div class="footer_inn">
<div id="menu3"> <?php //wp_nav_menu( array( 'theme_location' => 'm3','fallback_cb'=> '' ) ); ?> </div>
</div>
</div>
</div> -->

 
<!--<a id="scroll_to_top" title="<?php // _e('Scroll to top') ?>"> </a> -->

<!-- chili-web.com.ua -->
</footer>







<?php /* ********** Спливаючі вікна ************* */ ?>

<?php /* Яваскрипти перенесені в footer */ ?>

 <div id="overlay_2" class="overlay_fon" style="display: none; position: fixed; left: 0; right: 0; top: 0; bottom: 0; ">  </div> <!-- onClick="overlay_hide()" -->
  
  <?php /* Форма входу */ ?>
 <?php if (!is_user_logged_in()) : ?>
  <div id="form_login_mini" class="lightb_window medium" style="display: none;">
        <a class="close_but btn-remove" onClick="overlay_hide()" title="<?php _e('Close') ?>"> <i class="fa fa-close" aria-hidden="true"></i> </a>
		<div class="lightb_inner"> <?php include WOW_DIRE.'front_html_blocks/login_mini.php'; /* wow_e_shop *** login_mini *** */ ?> </div>
  </div> 
  <?php endif ?>
  
<div class="lightb_window big" id="lightb_cart" style="display: none;">
<a class="close_but btn-remove" onClick="overlay_hide()" title="<?php _e('Close') ?>"> <i class="fa fa-close" aria-hidden="true"></i> </a> 
<div class="lightb_inner"> </div>
</div>

<div class="lightb_window medium" id="lightb_contact_form_call_me" style="display: none;">
<a class="close_but btn-remove" onClick="overlay_hide()" title="<?php _e('Close') ?>"> <i class="fa fa-close" aria-hidden="true"></i> </a>
<div class="lightb_inner"> 
<div class="contact-form call_me">
<?php $form_name = 'contact_form_call_me'; ?>
<h4><?php _e('Call-back service') ?></h4>
<form name="<?php echo $form_name ?>" id="<?php echo $form_name ?>" method="post">
<ul class="c_form fields">
<li> <label for="call_customer_name"><?php _e('Name') ?></label> <div class="box"><input type="text" name="customer_name" id="call_customer_name" class="required" placeholder="<?php _e('Name') ?>" title="<?php _e('Name') ?>" value="" /></div> </li>
<li> <label for="call_customer_phone"><?php _e('Phone') ?></label> <div class="box"><input type="text" name="customer_phone" id="call_customer_phone" class="required" placeholder="<?php _e('Phone') ?>" title="<?php _e('Phone') ?>" value="" /></div> </li>
</ul>
<div class="but_line"><a class="button" onClick="do_contact_form('<?php echo $form_name ?>')"><span><?php _e('Submit') ?></span></a></div>
</form>
</div>
</div>
</div>

<?php /* **********  ************* */ ?>  






</div>
<!-- END: wrapper -->

<?php /* * jQuery завантажується асинхронно. Якщо jQuery ще не завантажився, вставляємо його додатково * */ ?>
<script type="text/javascript">
if (!window.jQuery) { document.write('<script src="<?php echo includes_url('/js/jquery/jquery.js'); ?>"><\/script>'); }
/// https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js 
// alert('no jQuery ...'); 
</script>


<script type="text/javascript">
<?php /* Головна стор. Слайдер. */ ?> 
// need jquery.cycle.js 
jQuery(document).ready(function($) {
if (jQuery().cycle) {
	// Main Slider
	jQuery('#main_slider_slides').cycle({ 
		fx:     'scrollHorz', // fade scrollHorz scrollVert zoom shuffle turnDown curtainX // rev: true
		speed:    700, 
		timeout: 4000, // 4200
		pause:   1,
		next:  '.slider_m.hslider-next',
		prev:  '.slider_m.hslider-prev',
		pager:  '.slider_m.controls',
		fit:  3,
		// slideResize:  0,
		// rev: true 
	});
}
});
</script>


<script type="text/javascript"> 

jQuery(document).ready(function($) {
	 $.fn.scrollToTop=function(){$(this).hide().removeAttr("href");
	 if($(window).scrollTop()!="0"){$(this).fadeIn("slow")}var scrollDiv=$(this);
	 $(window).scroll(function(){if($(window).scrollTop()<200){$(scrollDiv).fadeOut("slow")}
	 else{$(scrollDiv).fadeIn("slow")}});
	 $(this).click(function(){$("html, body").animate({scrollTop:0},"slow")})}
});

jQuery(document).ready(function($) {
$("#scroll_to_top").scrollToTop();
});


	jQuery(document).ready(function($) { <?php /* easyTooltip */ ?> 
		$("a").easyTooltip();
		$("label").easyTooltip();
		$("input").easyTooltip();
		$("button").easyTooltip();
	});


jQuery(document).ready(function($) { <?php /* Виїжджаюче меню */ ?>
$(".menu-hamb").click(function(){
		if ($(this).is(".open")) { 
	$(this).removeClass("open").addClass("close");
	$(this).next().addClass("expande");
	$(".top_menu ul.menu > li").removeClass("open").addClass("close"); 
	$('.menu-header-menu-container').hide();
		}
		else { $(this).removeClass("close").addClass("open"); 	$('.menu-header-menu-container').show();}

return false;
});

$(".top_menu li.menu-item-has-children > a").click(function(){
	if ($(this).parent().is(".open")) { $(this).parent().removeClass("open").addClass("close"); }
	else { 
		$(".top_menu ul.menu > li").removeClass("open").addClass("close");
		$(this).parent().removeClass("close").addClass("open"); 
	}	
	if(window.innerWidth < 900) { //// or is Android, IOs ...
	return false;
	}
});
$(".top_menu ul.menu li").click(function () {
        $(this).toggleClass('green-bg');
});


$(".toogle-b").click(function(){
if ($(this).parent().parent().is(".open")) {
$(this).parent().parent().removeClass("open").addClass("close"); 
} else { 
		$(".column .block").removeClass("open").addClass("close");
		$(".column .block .block-content").hide("normal");
		$(this).parent().parent().removeClass("close").addClass("open"); 
}
	$(this).parent().next().addClass("expande");	
	$(this).parent().next().slideToggle("normal");
	return false;
});

}); /// jQuery(document).ready(function($)

<?php if(is_home() || is_front_page() ) { ?> <?php } ?>
var lastScrollTop = 0;
window.onscroll = function() { 
	set_fixed_top9(); 
	infi_scroll();  <?php /* Infinite Scroll, load more items */ ?> 
}
 
window.onload = function() { set_fixed_top9(); } // window.scrollBy(0, -50); 

function set_fixed_top9() {
  var header_con = document.getElementById("top");
  var main_menu = header_con.getElementsByClassName("main-menu")[0];
  var menu_hamb = header_con.getElementsByClassName("menu_hamb");
  var scroll_y = document.body.scrollTop || document.documentElement.scrollTop;
  
  var header_height = header_con.offsetHeight;  var menu_height = main_menu.offsetHeight;
  var top_con_height = header_height - menu_height;
  var height57 = scroll_y - top_con_height - 22;

  if(window.innerWidth > 780) {
	if(scroll_y > lastScrollTop) { lastScrollTop = scroll_y; header_con.className = '';} else { lastScrollTop = scroll_y; header_con.className = 'fix_menu';}
  }
  else {
	//header_con.className = 'fix_menu';
 if (document.querySelector(".menu-hamb").classList.contains("open")) {header_con.className = 'fix_menu';} else {header_con.className = '';}//
  }
 
<?php /*  
header.fix_menu { padding-bottom: 50px; }
// 50px - висота фіксованого меню або інша висота, на яку буде зменшена висота хедера 
*/ ?>
}

<?php /* 
jQuery(document).ready(function($) {
var h = $(window).scrollTop();
var h_in = 95;
if (h >= h_in ) {
	$('.main-menu').addClass('fix_menu');
	var kontrol = true;
} else if (h < h_in ) {
	$('.main-menu').removeClass('fix_menu');
	var kontrol = false; 
}

$(window).on('scroll', function(event) { 
	h = $(window).scrollTop();
	if ((h >= h_in) && kontrol == false) {
		$('.main-menu').addClass('fix_menu');
		$('.main-menu').css('display', 'none');
		$('.main-menu').fadeIn(400);
		
		kontrol = true;
	} else if ((h < h_in ) && kontrol == true) {
		$('.main-menu').removeClass('fix_menu');
		kontrol = false;
	}
	return false;
});
});
*/ ?>
</script>

<script type="text/javascript">
function set_menu_clas4() {
	act_punkt = 0;
	var menu_ul = document.getElementById("menu1").getElementsByTagName("ul")[0];	
		var menu_li_arr = menu_ul.children;
		for (var i = 0; i < menu_li_arr.length; i++) {
				act_punkt = 0;
			sub_menu_cycl_4(menu_li_arr[i]); /////////
		//// 
		if(act_punkt == 1) { menu_li_arr[i].className += ' current-menu-parent'; }
		////
		} // for
}
function sub_menu_cycl_4(menu_li) {	
	if(menu_li.children.length > 1) {  // 
		var summenu_ul = menu_li.children[1]; var summenu_arr = summenu_ul.children; 		
			for (var i2 = 0; i2 < summenu_arr.length; i2++) {		
			var clas2 = summenu_arr[i2].className;  
if(clas2.indexOf("current-menu-parent") != -1 || clas2.indexOf("current-menu-item") != -1 || clas2.indexOf("current-category-ancestor") != -1 || clas2.indexOf("current-post-ancestor") != -1) { act_punkt = 1;  } 
			sub_menu_cycl_4(summenu_arr[i2]); /// !!!
			} // for 		
	} // if (menu_li.children.length > 1)	
}

setTimeout( 'set_menu_clas4()', 0 );
</script>


<?php include WOW_DIRE.'js/e_shop_scripts.php'; /* wow_e_shop *** e_shop_scripts *** */ ?>

<!-- <a class="various fancybox.iframe" href="http://www.youtube.com/embed/L9szn1QQfas?autoplay=1">Youtube (iframe)</a> -->
<?php wp_footer(); ?>
<?php /* 
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/owl.carousel.min.js"></script>
 */ ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/main-script.js"></script>
<script>
jQuery(document).ready(function($) {
	if ($('.various').hasClass("various")) {
		$(".various").fancybox({
			maxWidth: 800,
			maxHeight: 600,
			width: '70%',
			height: '70%',
		});
	}
});
</script>


</body>
</html>