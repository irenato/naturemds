<?php get_header(); ?>

<?php $a_id = get_the_id(); ?>

   <?php 
   $post_type = get_post_type( $post );
   // global $wp_query;
	$queried_object = $wp_query->queried_object;	

	/* ******* 'normal', 'categories_list', 'mixed' ******* */
	$taxo_view = $queried_object->term_view; // 'normal', 'categories_list', 'mixed'
   ?>
   
   <div class="category blog tax-<?php echo $queried_object->taxonomy; ?> type-<?php echo $post_type; ?> cat-<?php echo $queried_object->parent; ?> cat-<?php echo $queried_object->term_id; ?>">
     
    
   
   <div class="content"> 
   
   <?php // breadcrumbs
   //if (function_exists('breadcrumbs')) breadcrumbs(); ?>
 
   
 <div class="page_title category_title title_content">
 	<h1><?php echo $queried_object->name; ?></h1>
 </div>
<?php $cat_curr = $queried_object; ?>
<?php if($cat_curr->description) { ?>
<div class="cat_description maine">
<?php if($cat_curr->term_thumbnail) { ?>
<div class="cat_image"><?php echo wp_get_attachment_image( $cat_curr->term_thumbnail, 'medium-img' ) ?></div>
<?php } ?>
<div class="descr"><?php echo term_description($cat_curr->term_id, $cat_curr->taxonomy) ?> <?php // echo $cat_curr->description ?></div>
</div>
<?php } ?>

        
<?php if(in_array($taxo_view, array('categories_list', 'mixed'))) : ?>


<?php 
$term_id = $queried_object->term_id;
$taxonomy = $queried_object->taxonomy;
if(count(get_term_children($term_id, $taxonomy))) { $parent_id = $term_id; }
else { $parent_id = $queried_object->parent; }

$child_terms = get_terms( $taxonomy, array('parent' => $parent_id, 'hide_empty' => false) ); //
?>
<?php if(count($child_terms)) : ?> 
<div class="cms_cont maine">
<?php // $line_8_count = 2;
// $num_8 = 0;
?>
<ul class="child_cats">
<?php foreach ($child_terms as $cat) : ?>
<?php /* <?php if(($num_8 % $line_8_count) == 0) { ?><ul class="child_cats"><?php } ?>
<?php $num_8++; ?> */ ?>
<li class="cat">
<?php if($cat->term_thumbnail) { ?>
<div class="cat_image"><a href="<?php echo get_term_link($cat) ?>" title="<?php echo $cat->name ?>"><?php echo wp_get_attachment_image( $cat->term_thumbnail, 'medium-img' ) ?></a></div>
<?php } ?>
<div class="cat_info">
<?php $count_1 = WOW_categories_Func::get_term_post_count4($cat); // $count_1 = $cat->count; ?>
<h2><a href="<?php echo get_term_link($cat) ?>"><?php echo $cat->name ?></a> <span>(<?php echo $count_1 ?>)</span></h2>
<?php $child_terms_2 = get_terms( $cat->taxonomy, array('parent' => $cat->term_id, 'hide_empty' => false) ); ?>
<?php if(count($child_terms_2)) : ?>
<ul>
<?php foreach ($child_terms_2 as $cat_2) { ?>
<?php $count_2 = WOW_categories_Func::get_term_post_count4($cat_2); // $count_2 = $cat_2->count; ?>
<li><a href="<?php echo get_term_link($cat_2) ?>"><?php echo $cat_2->name ?></a> <span>(<?php echo $count_2 ?>)</span></li>
<?php } ?>
</ul>
<?php endif; // ($child_terms_2) ?>
</div>
</li>
<?php /* <?php if(($num_8 % $line_8_count) == 0 or $num_8 == count($child_terms)) { ?></ul><?php } ?> */ ?>
<?php endforeach; ?>
</ul>
</div>
<?php endif; // ($child_terms) ?>




<?php endif; // taxo_view: 'categories_list', 'mixed' ?>

<?php if($taxo_view != 'categories_list') : ?>
<?php // main content ?> <?php if(have_posts()) : ?>

<?php //include WOW_DIRE.'front_html_blocks/toolbar_sorter.php'; /* wow_e_shop *** toolbar_sorter *** */ ?>

<?php include WOW_DIRE.'front_html_blocks/sidebar_filter_f_select.php'; // Фільтри формату select  */ ?>

<div class="grid_cont maine">

<?php 
// $view_mode = WOW_Product_List_Func::get_view_mode();
?>
<?php // if($view_mode == 'grid') : /* ******** ******  grid  ***** ******** */ 
// $wp_query->post_count; 
?>
<ul id="content-list" class="products-grid cols_4 ajax_infi_replace2"> 
  <?php while(have_posts()) : the_post(); ?>    

<li class="item"> <?php // content ?> 
 <?php $post_id = $post->ID;
  include WOW_DIRE.'front_html_blocks/sticker.php'; /* wow_e_shop *** sticker *** */ ?> 
 <div class="inn_cont">
 <h3 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
 <a class="product-image" href="<?php the_permalink(); ?>" title="<?php // the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'medium-img' ); } else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/ntrmds_icons/client.svg" class="no_feat" /> </div>'; } ?></a>      
    <?php $post_id = $post->ID;
  include WOW_DIRE.'front_html_blocks/attributes.php'; /* wow_e_shop *** attributes *** */ ?>
 
 </div>
</li>
 
 <?php endwhile; // posts query ?>
</ul> 
 

 <?php // else: // /* ********  view_mode == 'list'  ******** */ ?>

 <?php // endif; // $view_mode ?>

</div> <!-- grid_cont -->


<?php else : // no posts ?> 

<div class="conte maine">
 <article class="no-posts"> <p> <?php _e( 'There are no products matching the selection.' ); ?> </p> </article>
</div> 
 
  <?php endif; ?>	<?php // -//- end main content ?>    
   		

	<?php if($wp_query->max_num_pages > 1) { ?> <?php /* Infinite Scroll, load more items */ ?>
<?php /* <div class="more_line"> <a class="button show-more" onclick="show_more_items(this)"><?php _e('More...'); ?></a> </div> */ ?>
	<?php } ?>
    <?php /* Infinite Scroll - footer.php: window.onscroll = function() { set_fixed_top9(); infi_scroll(); } */ ?>
	
	<?php if (function_exists('wp_corenavi')) wp_corenavi(''); ?> <?php /* don"t delete this; you can use "display: none;" */ ?>
    
    
<?php endif; // taxo_view: 'normal', 'mixed' ?>




<?php /* **** __end wow_e_shop zone */ ?>
	
 
 </div> <!-- content -->
 
            
    
</div> <!-- class="category blog" -->
   


<?php get_footer(); ?>