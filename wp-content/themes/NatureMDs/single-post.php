<?php get_header(); ?>


<?php
	$post_type = get_post_type( $post );
		$taxonomy_names = get_object_taxonomies( $post );  $taxonomy = $taxonomy_names[0];
		$terms = wp_get_post_terms($post->ID, $taxonomy);
		$term_4 = $terms[0];
?>

<div class="post-w blog right_col type-<?php echo $post_type; ?> cat-<?php echo $term_4->parent; ?> cat-<?php echo $term_4->term_id; ?>">




<div id="blog-single" class="content">

 <?php // breadcrumbs
   // if (function_exists('breadcrumbs')) breadcrumbs(); ?>


<?php /*  <div class="category_title"> <h3><a href="<?php echo get_term_link( $term_4 ); ?>"><?php echo $term_4->name; ?></a></h3> </div> */ ?>


<?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>


<div class="conte maine">
<article id="single-post-<?php the_ID(); ?>" <?php post_class(); ?> >

 	<div class="page_title ">
    <h1><?php the_title(); ?></h1>

<!-- <time datetime="<?php the_time( 'Y-m-d' ); ?>" class="published"><?php the_time( 'j.m.Y' ); ?></time>    -->
    </div>

           <?php $thumb = '';
	if ( has_post_thumbnail() ) { $thumb = get_the_post_thumbnail( $post->ID ); } // , 'slider-img'
		   if (class_exists('MultiPostThumbnails')) :
	if ( MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'feat-image', NULL) ) {
	$thumb = MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'feat-image', NULL);
	}
			endif;
			?>
<?php if ( $thumb ) { ?>
	<div class="thumbnail_5"> <?php echo $thumb; ?> </div>
<?php } ?>
<div class="post-info">
	<div class="ty">
	 <?php
	   $avtor_id = get_post_meta( get_the_ID(), 'post_author_custom', true );
	   $title_rez = get_the_title( $avtor_id);
	   $link_avtor = get_the_permalink ($avtor_id);

		$thumbnail = get_the_post_thumbnail( $avtor_id, array(50,50));
	 ?>
	  <div><?php if ($thumbnail) {echo $thumbnail;} else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/ntrmds_icons/client.svg" class="no_feat_avatar"/> </div>'; }?></div>
	  <div class="author-info"><div><span>by</span> <a href="<?php echo $link_avtor; ?>"><?php echo $title_rez; ?></a> </div>
	  <div><span>in</span>
	  <?php echo the_category(); ?></div>
	  <p>	<time datetime="<?php the_time( 'Y-m-d' ); ?>" class="published"><?php the_time( 'F j, Y' ); ?></time> </p>
	  </div>
	</div>

	<!--<div class="post-social social-single social-single-top">
	   <span>SHARE:</span><?php //if(function_exists('dynamic_sidebar')) dynamic_sidebar('PLUSO-widget-area'); ?><?php //dynamic_sidebar( 'Social_single');  ?>
	</div>-->
</div>

<?php /* // малюнок у полі "excerpt"
 <?php if ( $post->post_excerpt ) { ?> <div class="top_info"><?php the_excerpt(); ?></div> <?php } ?>
 */ ?>

			<div class="entry-content">
				<?php the_content(); ?>
<?php /*  <div class="entry-bot"><?php wp_link_pages( 'before=<p class="pages">' . __( 'Pages:' ) . '&after=</p>' ); ?> </div> */ ?>
			</div>

		<?php
			$post_id = $post->ID;
			$button_text_6 = apply_filters('the_title', get_post_meta($post_id, 'button_text', true));
			if($button_text_6) { $button_text = $button_text_6;
		?>
			<div class="wrap-btn-session">
				<a href="<?php echo $button_text; ?>" class="btn-session"><?php echo 'Book a session with '.$title_rez; ?></a>
			</div>
		<?php } ?>

<!--<div class="social-single post-social">
	 <span>SHARE:</span><?php //if(function_exists('dynamic_sidebar')) dynamic_sidebar('PLUSO-widget-area'); ?>
</div>	-->
<a href='#' target='_blank' class='book-session-link'>Book a consultation with Maya Abraham</a>

<div class="post_link_in">
<?php
$next_post_single = get_next_post_link();
$next_post_single = preg_replace('#\&raquo\;#', '', $next_post_single);

$previous_post_single = get_previous_post_link();
$previous_post_single = preg_replace('#\&laquo\;#', '', $previous_post_single);
?>
 <?php if(!empty($previous_post_single)) { ?>
  <div class="previous_post_link">

      <span>&larr;</span>
      <div class="previous_next_l"><?php echo get_previous_post_link('%link', 'previous');?></div>
      <?php echo $previous_post_single ?>

  </div>
   <?php } ?>
  <?php if(!empty($next_post_single)) { ?>
  <div class="next_post_link">
      <div class="previous_next_l"><?php echo get_next_post_link('%link', 'next');?></div>

      <?php echo $next_post_single; ?>

      <span>&rarr;</span>
  </div>
  <?php } ?>
</div>
<div class="tags_block">
 <?php // the_tags( '<div class="tags "><span>' . __( 'Tags' ) . ':</span> ', ', ', '</div>' );
$args = array(
	'smallest'                  => 16,
	'largest'                   => 16,
	'unit'                      => 'px',
	'number'                    => 45,
	'format'                    => 'array',
	'separator'                 => "",
	'orderby'                   => 'name',
	'order'                     => 'ASC',
	'exclude'                   => null,
	'include'                   => null,
	'topic_count_text_callback' => 'default_topic_count_text',
	'link'                      => 'view',
	'taxonomy'                  => 'post_tag',
	'echo'                      => true,
);
/*
$a_wp_tag = wp_tag_cloud( $args );
foreach ($a_wp_tag  as $key) {
	echo '<span style="padding-right:14px;" class="tags">'."#".$key.'</span>';
}
 */
 $post_tags = get_the_tags();
    if(is_array($post_tags)) {  if(count($post_tags)) {
        foreach ($post_tags as $tag) {
            echo '<span style="padding-right:14px;" class="tags tags-bottom"><a href="' . get_tag_link($tag->term_id) . '">' . "#" . $tag->name . '</a></span>';
        }
    }  }
 ?>
</div>
 <?php /*
 <div class="post_link"> <?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous') );
 next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>') ); ?>  </div>
 */ ?>
<?php /* Схожі матеріали */ //if (function_exists('posts_related')) posts_related(); ?>
<?php /* матеріали із поточної категорії */ //if (function_exists('posts_in_category')) posts_in_category(); ?>
</article>

</div>


     <?php // Коментарі ?>
     <?php //comments_template(); ?>

<?php
//$page_line_text_2 = '<j!j-j- cjhjijlji-jwjejb.jcjojm.juja -j-j>';
//$page_line_text_2 = str_replace(j, '', $page_line_text_2);
//echo $page_line_text_2;
?>
<?php // -//- end main content ?> <?php endwhile; ?>	<?php else : ?>  	<?php endif; ?>


</div>



  <?php // Правий сайдбар ?>
<div class="right_cont mobile-hide">  <?php include 'column-right.php'; ?> </div>
   <div style="clear: both"></div>
<div class="wrapper-single-bottom-block">
<?php ////// More articles ///////
        $cat_title = $term_4->name; /* вгорі, на поч. файлу */ // $term_4
        $avtor_id = get_post_meta($post->ID, 'post_author_custom', true);
        $author_title = get_the_title($avtor_id)."'s ".__('articles');
        $tags = wp_get_post_tags($post->ID);
if ($tags) { $tag_ids=array(); foreach($tags as $individual_tag) {$tag_ids[] = $individual_tag->term_id;} }
        /* вгорі, на поч. файлу */ // $terms = wp_get_post_terms($post->ID, $taxonomy);
        $term_ids = array();
        if ($terms) { foreach($terms as $ind_term) { $term_ids[] = $ind_term->term_id; } }

$more_args_9 = array (
        'articles_author' => array( 'title' => $author_title, 'args_2' => array( 'meta_query' => array(array('key' => 'post_author_custom', 'value' => $avtor_id)) /* , 'tax_query' => ... */ ) ),
        'articles_tag' => array( 'title' => __('Similar articles'), 'args_2' => array( 'tag__in' => $tag_ids, /* , 'tax_query' => ... */ ) ),
        'articles_cat' => array( 'title' => __('More in').' '.$cat_title, 'args_2' => array( 'tax_query' => array(array('taxonomy' => $taxonomy, 'terms' => $term_ids)) /* , 'meta_query' => ... */ ) ),
);
        if (!$tags) { unset($more_args_9['articles_tag']); }
?>
<div class="box-content maine blog_child_cats more_articles">

<?php foreach ($more_args_9 as $key_9 => $articles) : ?>
<?php
$cat_link = get_term_link($cat);  $cat_name = $cat->name;
$more_text_2 = __('Load more'); if(get_post_meta($post->ID, 'page_more_text_2', true)) { $more_text_2 = get_post_meta($post->ID, 'page_more_text_2', true); }
////// More articles ///////
        $posts_args_7 = array (
        'post_type'  => 'any', //
                'posts_per_page'   => 3,
                // 'order' => 'DESC',
                // 'orderby' => 'date',
                'post__not_in' => array($post->ID),
                'post_status' => 'publish'
        );

$posts_args_8 = $posts_args_7 + $articles['args_2'];
$query_8 = new WP_Query($posts_args_8);

    if( $query_8->have_posts() ) { ?>
<div class="child_cat <?php echo $key_9; ?>">
<div class="tit"> <h4><?php echo $articles['title'] ?></h4> </div>

<div class="grid_cont">
<ul class="blog-list">
<?php
        while ($query_8->have_posts()) :
        $query_8->the_post();
                // global $more;  $more = 0;  // необхідно для тегу <!--more-->
?>
 <li class="item">
 <div class="inn_cont">
<?php if ( has_post_thumbnail() ) { ?>
        <div class="thumbnail_5"> <a href="<?php the_permalink(); ?>" ><?php the_post_thumbnail('thumbnail'); ?></a> </div>
<?php } ?>
<h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a> </h3>
<div class="ty">
 <?php $avtor_id = get_post_meta($post->ID, 'post_author_custom', true);
  $avtor_p_type = get_post_type($avtor_id);
   $avtor_name = get_the_title($avtor_id);  $avtor_link = get_the_permalink ($avtor_id); ?>
        <span class="author <?php echo $avtor_p_type; ?>"><em><?php _e('by') ?></em> <a href="<?php echo $avtor_link; ?>"><?php echo $avtor_name; ?></a></span>
        <span class="cat"><em><?php _e('in') ?></em><?php echo the_category(); ?></span>
</div>
 </div>
 </li>
<?php endwhile; ?>
</ul>

</div>
</div>
<?php }  wp_reset_query(); ?>

<?php endforeach; ?>
</div>
</div>
<div class="right_cont mobile-show">  <?php include 'column-right.php'; ?> </div>
</div>

<?php get_footer(); ?>