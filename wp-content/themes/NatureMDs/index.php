<?php /* ***** *** 1 product in category */
/// if( $wp_query->post_count == 1 ) { wp_safe_redirect( get_permalink($post->ID) ); }
/*$id_n = get_queried_object_id();
if($id_n == 5){
	 include TEMPLATEPATH."/consultant-cat.php";
	exit();
}*/
?>

<?php get_header(); ?>

<?php $a_id = get_the_id(); ?>

   <?php 
   $post_type = get_post_type( $post );
   // global $wp_query;
	$queried_object = $wp_query->queried_object;	

	/* ******* 'normal', 'categories_list', 'mixed' ******* */
	$taxo_view = $queried_object->term_view; // 'normal', 'categories_list', 'mixed'
   ?>
   
   <div class="category no_column blog tax-<?php echo $queried_object->taxonomy; ?> type-<?php echo $post_type; ?> cat-<?php echo $queried_object->parent; ?> cat-<?php echo $queried_object->term_id; ?>">
     
   
   
   <div class="content"> 
   
   <?php // breadcrumbs
   //if (function_exists('breadcrumbs')) breadcrumbs(); ?>
 
   
 <div class="page_title category_title title_content">
 	<h1><?php echo $queried_object->name; ?></h1>
 </div>

        
<?php if(in_array($taxo_view, array('categories_list', 'mixed'))) : ?>


<?php 
$term_id = $queried_object->term_id;
$taxonomy = $queried_object->taxonomy;
if(count(get_term_children($term_id, $taxonomy))) { $parent_id = $term_id; }
else { $parent_id = $queried_object->parent; }

$child_terms = get_terms( $taxonomy, array('parent' => $parent_id, 'hide_empty' => false) ); //
?>
<?php if(count($child_terms)) : ?> 
<div class="cms_cont maine">
<?php // $line_8_count = 2;
// $num_8 = 0;
?>
<ul class="child_cats">
<?php foreach ($child_terms as $cat) : ?>
<?php /* <?php if(($num_8 % $line_8_count) == 0) { ?><ul class="child_cats"><?php } ?>
<?php $num_8++; ?> */ ?>
<li class="cat">
<?php if($cat->term_thumbnail) { ?>
<div class="cat_image"><a href="<?php echo get_term_link($cat) ?>" title="<?php echo $cat->name ?>"><?php echo wp_get_attachment_image( $cat->term_thumbnail, 'medium-img' ) ?></a></div>
<?php } ?>
<div class="cat_info">
<?php $count_1 = WOW_categories_Func::get_term_post_count4($cat); // $count_1 = $cat->count; ?>
<h2><a href="<?php echo get_term_link($cat) ?>"><?php echo $cat->name ?></a> <span>(<?php echo $count_1 ?>)</span></h2>
<?php $child_terms_2 = get_terms( $cat->taxonomy, array('parent' => $cat->term_id, 'hide_empty' => false) ); ?>
<?php if(count($child_terms_2)) : ?>
<ul>
<?php foreach ($child_terms_2 as $cat_2) { ?>
<?php $count_2 = WOW_categories_Func::get_term_post_count4($cat_2); // $count_2 = $cat_2->count; ?>
<li><a href="<?php echo get_term_link($cat_2) ?>"><?php echo $cat_2->name ?></a> <span>(<?php echo $count_2 ?>)</span></li>
<?php } ?>
</ul>
<?php endif; // ($child_terms_2) ?>
</div>
</li>
<?php /* <?php if(($num_8 % $line_8_count) == 0 or $num_8 == count($child_terms)) { ?></ul><?php } ?> */ ?>
<?php endforeach; ?>
</ul>
</div>
<?php endif; // ($child_terms) ?>


<?php include WOW_DIRE.'front_html_blocks/front_products_sliders.php'; /* wow_e_shop *** products_sliders *** */ ?>  


<?php endif; // taxo_view: 'categories_list', 'mixed' ?>

<?php if($taxo_view != 'categories_list') : ?>
<?php // main content ?> <?php if(have_posts()) : ?>

<?php //include WOW_DIRE.'front_html_blocks/toolbar_sorter.php'; /* wow_e_shop *** toolbar_sorter *** */ ?>

<?php include WOW_DIRE.'front_html_blocks/sidebar_filter_f_select.php'; // Фільтри формату select  */ ?>

<div class="grid_cont maine">

<?php 
$view_mode = WOW_Product_List_Func::get_view_mode();
?>
<?php if($view_mode == 'grid') : /* ******** ******  grid  ***** ******** */ 
// $wp_query->post_count; 
?>
<ul id="content-list" class="products-grid ajax_infi_replace2">  
  <?php while(have_posts()) : the_post(); ?>    

<li class="item"> <?php // content ?>
 <?php $post_id = $post->ID;
  include WOW_DIRE.'front_html_blocks/sticker.php'; /* wow_e_shop *** sticker *** */ ?> 
 <div class="inn_cont">
 <a class="product-image" href="<?php the_permalink(); ?>" title="<?php // the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'medium-img' ); } else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/ntrmds_icons/client.svg" class="no_feat" /> </div>'; } ?></a> 
 <?php /*  <?php include WOW_DIRE.'front_html_blocks/media_section.php'; // media_section ?> */ ?>
 <h3 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <div class="addto">
        <div class="addto-main">
		<?php $product_price = WOW_Attributes_Front::product_get_price(); ?>
        <div class="price-box"><?php echo $product_price ?></div>    
		<?php $stock_2 = get_post_meta ($post->ID, 'stock', true); ?>
					<?php if($stock_2 > 0 or $stock_2 == '') : ?>
     	<?php $product_type = get_post_meta ($post->ID, 'product_type', true); ?>
            <div class="addtocart"> <a <?php if($product_type == 'configurable') { ?>href="<?php the_permalink(); ?>"<?php } else { ?>onclick="addtocart('<?php the_ID() ?>', '1')"<?php } ?> class="button btn-cart"><?php _e('Add to cart') ?></a> </div>
                    <?php else: ?>
             <div class="availability out-of-stock"><span><?php _e('Out of stock') ?></span></div>
                    <?php endif; ?>
        </div>
            <div class="addto-links">
            <div class="link"><a class="compare" onclick="addto_compare('<?php the_ID() ?>')" title="<?php _e('Add to compare') ?>"><i class="fa fa-exchange<?php /* fa fa-bar-chart */ ?>" aria-hidden="true"></i> <span><?php _e('Add to compare') ?></span></a></div>
            <div class="link"><a class="wishlist" onclick="addto_wishlist('<?php the_ID() ?>')" title="<?php _e('Add to wishlist') ?>"><i class="fa fa-check-square-o" aria-hidden="true"></i> <span><?php _e('Add to wishlist') ?></span></a></div>
            </div>
	</div> <!-- addto -->
    
    <?php $post_id = $post->ID;
  include WOW_DIRE.'front_html_blocks/attributes.php'; /* wow_e_shop *** attributes *** */ ?>
 
 </div>
</li>
 
 <?php endwhile; // posts query ?>
</ul> 
 

 <?php else: // /* ********  view_mode == 'list'  ******** */ ?>

 <ul id="content-list" class="products-list ajax_infi_replace2">
  <?php while(have_posts()) : the_post();  $post_types2 = get_post_type( $post->ID );   ?>  

<li class="item item-consultant"> <?php // content ?> 
 <?php $post_id = $post->ID;
  include WOW_DIRE.'front_html_blocks/sticker.php'; /* wow_e_shop *** sticker *** */   ?>  
 <div class="inn_cont">
 <a class="product-image" href="<?php the_permalink(); ?>" title="<?php // the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'medium-img' ); } else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/ntrmds_icons/client.svg" class="no_feat" /> </div>'; } ?></a>
 <?php /*  <?php include WOW_DIRE.'front_html_blocks/media_section.php'; // media_section ?> */ ?>
 
 <div class="prod-center consultant-attr">
 <h3 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
 <?php /* <div class="entry-content"><?php the_content(); ?></div> */ ?>
 <?php $post_id = $post->ID; 
  include WOW_DIRE.'front_html_blocks/attributes.php'; /* wow_e_shop *** attributes *** */ 
  ?>

<div class="bl-info">
	<span class="bl"><a class="profil" href="<?php the_permalink(); ?>" title=""><img src="<?php echo get_bloginfo('template_url').'/images/ntrmds_icons/client.svg'; ?>"><?php _e('Profile'); ?></a></span>
<?php // $prod_video = get_post_meta($post->ID, 'prod_video', true); 
$prod_video_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'prod_video'); ?>
    <?php if($prod_video_6['atr_value']) : 
	$prod_video = implode(', ', $prod_video_6['atr_value']); ?>
	<span class="bl"><a class="video various fancybox.iframe" href="<?php echo $prod_video; ?>"><img src="<?php echo get_bloginfo('template_url').'/images/ntrmds_icons/camera.svg'; ?>"><?php echo $prod_video_6['frontend_label'] ?></a></span> 
	<?php endif; ?>
<?php $sessionfee_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'sessionfee'); ?>
    <?php if($sessionfee_6['atr_value']) : 
	$sessionfee = implode(', ', $sessionfee_6['atr_value']); ?>
	<span class="bl bl-fee"><?php echo $sessionfee_6['frontend_label'] ?><span>:</span> <?php echo $sessionfee ?> </span> 
	<?php endif; ?> 
</div>

 </div>
     <div class="addto">
<?php // $schedule = get_post_meta($post->ID, 'schedule_session', true); 
$schedule_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'schedule_session'); ?>
    <?php if($schedule_6['atr_value']) : 
	$schedule = implode(', ', $schedule_6['atr_value']); ?>
 <a target="_blank" href="<?php echo $schedule ?>" class="btn-session"><?php echo $schedule_6['frontend_label'] ?></a> 
	<?php endif; ?>  
       </div>  <!-- addto -->

 </div>
 </li>
 
 <?php endwhile; // posts query ?> 
 
 </ul> <!-- products-list -->

 <?php endif; // $view_mode ?>

</div> <!-- grid_cont -->


<?php else : // no posts ?> 

<div class="conte maine">
	<?php if ($post_type=='consultant') {
		include WOW_DIRE.'front_html_blocks/sidebar_filter_f_select.php'; // Фільтри формату select  */?>
		<article class="no-posts"> <p> <?php _e( "No consultants matched your search. Please try a new search." )?> </p> </article>
	<?php } else{?>
 <article class="no-posts"> <p> <?php _e( 'There are no products matching the selection1.' ); ?> </p> </article>
	<?php }?>
</div> 
 
  <?php endif; ?>	<?php // -//- end main content ?>    
   		

	<?php if($wp_query->max_num_pages > 1) { ?> <?php /* Infinite Scroll, load more items */ ?>
<?php /* <div class="more_line"> <a class="button show-more" onclick="show_more_items(this)"><?php _e('More...'); ?></a> </div> */ ?>
	<?php } ?>
    <?php /* Infinite Scroll - footer.php: window.onscroll = function() { set_fixed_top9(); infi_scroll(); } */ ?>
	
	<?php if (function_exists('wp_corenavi')) wp_corenavi(''); ?> <?php /* don"t delete this; you can use "display: none;" */ ?>
    
    
<?php endif; // taxo_view: 'normal', 'mixed' ?>


<?php $cat_curr = $queried_object; ?>
<?php if($cat_curr->description) { ?>
<div class="cat_description maine">
<?php if($cat_curr->term_thumbnail) { ?>
<div class="cat_image"><?php echo wp_get_attachment_image( $cat_curr->term_thumbnail, 'medium-img' ) ?></div>
<?php } ?>
<div class="descr"><?php echo term_description($cat_curr->term_id, $cat_curr->taxonomy) ?> <?php // echo $cat_curr->description ?></div>
</div>
<?php } ?>


<?php /* **** __end wow_e_shop zone */ ?>
	
 
 </div> <!-- content -->
 
            
    
</div> <!-- class="category blog" -->
   


<?php get_footer(); ?>