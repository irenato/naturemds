<?php get_header(); ?>


   <?php
   $post_type = get_post_type( $post );
   // global $wp_query;
	$queried_object = $wp_query->queried_object;
   ?>

   <div class="tag category blog right_col tax-<?php echo $queried_object->taxonomy; ?> type-<?php echo $post_type; ?> cat-<?php echo $queried_object->parent; ?> cat-<?php echo $queried_object->term_id; ?>">


<div class="page_title category_title title_content"> <h1><?php echo $queried_object->name; ?></h1> </div>

	<div class="new-menu blog_menu">
		<?php wp_nav_menu(array('theme_location' => 'm4','container' => '')); ?>
	</div>

       <script>
           jQuery(document).ready(function($1) {
               var mql = window.matchMedia('all and (max-width: 550px)');
               if (mql.matches) {
                   $1(".page_title").css( "cursor", "pointer" );
                   $1(".page_title").click(function () {

                       $1(".new-menu").slideToggle( "fast", function() {
                           // Animation complete.
                       });
                   });
               } else {

               }
           });
       </script>

   <div class="content">

   <?php // breadcrumbs
   // if (function_exists('breadcrumbs')) breadcrumbs(); ?>

<div class="grid_cont maine">
 <ul id="content-list" class="products-grid cols_2 blog-archive ajax_infi_replace2">
  <?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

 <li class="item">
 <div class="inn_cont">
<?php if ( has_post_thumbnail() ) { ?>
	<div class="thumbnail_5"> <a href="<?php the_permalink(); ?>" ><?php the_post_thumbnail( 'main-img' ); ?></a> </div>
<?php } ?>
<h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a> </h3>
<div class="ty">
 <?php $avtor_id = get_post_meta($post->ID, 'post_author_custom', true);
  $avtor_p_type = get_post_type($avtor_id);
   $avtor_name = get_the_title($avtor_id);  $avtor_link = get_the_permalink ($avtor_id); ?>
	<span class="author <?php echo $avtor_p_type; ?>"><em><?php _e('by') ?></em> <a href="<?php echo $avtor_link; ?>"><?php echo $avtor_name; ?></a></span>
	<span class="cat"><em><?php _e('in') ?></em><?php echo the_category(); ?></span>
</div>
<?php $cutti_num = 200;
// $short_content_2 = preg_replace('`\[[^\]]*\]`', '', strip_tags(get_the_content())); //
$short_content_2 = strip_shortcodes( strip_tags(get_the_content()) ); // WP function "strip_shortcodes"
$charset = get_bloginfo('charset'); // $charset = 'UTF-8';
$short_content = mb_substr($short_content_2, 0, $cutti_num, $charset);
$short_content = mb_substr($short_content, 0, strripos($short_content, ' ', 0, $charset), $charset);
?>
          <div class="descr entry-content"> <p><?php echo $short_content; ?></p> </div>
 </div>
 </li>

 <?php endwhile; ?>	<?php else : ?>
<div class="conte maine">
 <article class="no-posts"> <p> <?php _e( 'Sorry, no posts matched your criteria.' ); ?> </p> </article>
</div>

 <?php // -//- end main content ?> <?php endif; ?>
 </ul>
</div>

	<?php if($wp_query->max_num_pages > 1) { ?> <?php /* Infinite Scroll, load more items */ ?>
<?php /* <div class="more_line"> <a class="button show-more" onclick="show_more_items(this)"><?php _e('More...'); ?></a> </div> */ ?>
	<?php } ?>
    <?php /* Infinite Scroll - footer.php: window.onscroll = function() { set_fixed_top9(); infi_scroll(); } */ ?>

    <?php if (function_exists('wp_corenavi')) wp_corenavi(''); ?> <?php /* don"t delete this; you can use "display: none;" */ ?>


 </div> <!-- content -->



     <?php // Правий сайдбар ?>
     <?php include 'column-right.php'; ?>


</div> <!-- class="category blog" -->



<?php get_footer(); ?>