<?php get_header(); ?>

<?php
	$post_type = get_post_type( $post );
		$taxonomy_names = get_object_taxonomies( $post );  $taxonomy = $taxonomy_names[0];
		$terms = wp_get_post_terms($post->ID, $taxonomy);
		$term_4 = $terms[0];
?>


<div class="post-w blog type-<?php echo $post_type; ?> cat-<?php echo $term_4->parent; ?> cat-<?php echo $term_4->term_id; ?>" style="padding-left: 0;">


<div class="content">


 <?php // breadcrumbs
   // if (function_exists('breadcrumbs')) breadcrumbs(); ?>


<?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>


<div class="page_title category_title"> <h1><?php the_title(); ?></h1> </div>


<?php /* *** start wow_e_shop zone */ ?>
<div class="product-view" id="p-<?php the_ID() ?>">

<?php $post_id = $post->ID;
$post_id_gen = $post->ID; ?>


 <?php if ( $post->post_excerpt ) { ?> <div class="right_info"><?php the_excerpt(); ?></div> <?php } ?>

<div class="entry-content sidor"><?php the_content(); ?></div>


<?php // $dohtor = get_post_meta($post->ID, 'what_btn', true);
$dohtor_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'what_btn'); ?>
    <?php if($dohtor_6['atr_value']) :
	$dohtor = implode(', ', $dohtor_6['atr_value']); ?>
<div class="bott_line">
 <a target="_blank" href="<?php echo $dohtor ?>" class="btn-session btn-health"><?php echo $dohtor_6['frontend_label'] ?></a>
</div>
	<?php endif; ?>


</div> <!-- product-view -->
<?php /* **** __end wow_e_shop zone */ ?>




     <?php // Коментарі ?>
     <?php // comments_template(); ?>


<?php // -//- end main content ?> <?php endwhile; ?>	<?php else : ?>  	<?php endif; ?>



<div class="post_link_in">
<?php
$next_post_single = get_next_post_link();
$next_post_single = preg_replace('#\&raquo\;#', '', $next_post_single);

$previous_post_single = get_previous_post_link();
$previous_post_single = preg_replace('#\&laquo\;#', '', $previous_post_single);
?>
 <?php if(!empty($previous_post_single)) { ?>
  <div class="previous_post_link">
      <div class="previous_next_l"><?php echo get_previous_post_link('%link', 'previous');?></div>
    <span>&larr;</span><?php echo $previous_post_single; ?>
  </div>
   <?php } ?>
  <?php if(!empty($next_post_single)) { ?>
  <div class="next_post_link">
      <div class="previous_next_l"><?php echo get_next_post_link('%link', 'next');?></div>
     <?php echo $next_post_single; ?><span>&rarr;</span>
  </div>
  <?php } ?>
</div>






</div> <!-- content -->





</div>






<?php get_footer(); ?>