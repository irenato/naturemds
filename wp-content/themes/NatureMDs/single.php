<?php get_header(); ?>

<?php 
	$post_type = get_post_type( $post );   
		$taxonomy_names = get_object_taxonomies( $post );  $taxonomy = $taxonomy_names[0];
		$terms = wp_get_post_terms($post->ID, $taxonomy);
		$term_4 = $terms[0];
?>


<div class="post-w no_column blog type-<?php echo $post_type; ?> cat-<?php echo $term_4->parent; ?> cat-<?php echo $term_4->term_id; ?>">


<div class="content">


 <?php // breadcrumbs
   // if (function_exists('breadcrumbs')) breadcrumbs(); ?>
   
 

<div class="category_title"> 
<h3><?php if ($post_type=='consultant') {echo 'Consultant profile';} else {echo get_post_type_object($post_type)->labels->name;} ?></h3> 
<?php /* <?php if($_SERVER['HTTP_REFERER']) { ?>
<div class="go_back"><a class="button back" href="<?php echo $_SERVER['HTTP_REFERER'] ?>"><?php _e('View all') ?><?php // _e('Show all') ?></a></div>
<?php } ?> */ ?>
</div>



<?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
 

<?php /* *** start wow_e_shop zone */ ?>
<div class="product-view" id="p-<?php the_ID() ?>">

<?php $post_id = $post->ID;
$post_id_gen = $post->ID; ?>

<?php $product_type = get_post_meta($post->ID, 'product_type', true); ?>


<?php // $post_id; // $post_id_gen 
  include WOW_DIRE.'front_html_blocks/sticker.php'; /* wow_e_shop *** sticker *** */ ?>


<div class="product-shop type-<?php echo $product_type ?>">


<div class="cons-left">    
<?php //include WOW_DIRE.'front_html_blocks/media_section.php'; /* wow_e_shop *** media_section *** */ ?>
<a class="product-image" href="<?php the_permalink(); ?>" ><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'medium-img' ); } else { echo '<div class="inn"> <img src="'.get_template_directory_uri().'/images/no_feat_image.png" class="no_feat" /> </div>'; } ?></a>

<?php /* 
social_facebook
social_vk
social_twitter
social_google_plus
social_vimeo
social_youtube
social_tumblr
social_instagram
social_skype
*/ ?>
<?php $social_attr_5 = WOW_Attributes_Front::post_view_my_group_attributes($post_id, 'social'); ?>
            <?php if(count($social_attr_5)) : ?>
            <div class="social-single"> 
            <ul> 
    <?php foreach ($social_attr_5 as $attribute) : ?> 
<?php $a_code = str_replace('social_', '', $attribute['code']);  $a_code = str_replace('_', '-', $a_code); 
$value = implode(", ", $attribute['atr_value']); 
if($a_code == 'skype') { $value = 'skype:'.$value.'?chat'; } ?>
    <li class="my_atr_item <?php echo $attribute['code'] ?>">
    <a href="<?php echo $value ?>" target="_blank">
    <i class="fa fa-<?php echo $a_code ?>"></i>   
     </a>
     </li>
     <?php endforeach; ?>    
            </ul>
            </div>  
			<?php endif; ?>

  
</div>

<div class="prod-main maine">


<?php // $short_descr = get_post_meta($post->ID, 'short_description', true); 
$short_descr_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'short_description');
?>
    <?php if($short_descr_6['atr_value']) : 
	$short_descr = implode(', ', $short_descr_6['atr_value']); ?>
    <div class="short-descr">
    <h4><?php echo $short_descr_6['frontend_label'] ?></h4>
	<div class="entry-content"><?php echo $short_descr ?></div>
    </div>
	<?php endif; ?>	
    	
	
  

        
	<div id="product-information" class="atr_table ajax_replace2_content">
	<div class="page_title"> <h1><?php the_title(); ?></h1> </div>
	<?php // $post_id;  // $post_id_gen;
  include WOW_DIRE.'front_html_blocks/attributes.php'; /* wow_e_shop *** attributes *** */ ?>

<div class="cons-attr">
<div class="bl-info">
	<?php //if ($post_type!='consultant') {?><!--<span class="bl"><a class="profil" href="<?php //the_permalink(); ?>" title=""><img src="<?php //echo get_bloginfo('template_url').'/images/profile.png'; ?>"><?php //_e('Profile'); ?></a></span>--><?php //}?>
<?php // $prod_video = get_post_meta($post->ID, 'prod_video', true); 
$prod_video_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'prod_video'); ?>
    <?php if($prod_video_6['atr_value']) : 
	$prod_video = implode(', ', $prod_video_6['atr_value']); ?>
	<span class="bl"><a class="video various fancybox.iframe" href="<?php echo $prod_video; ?>"><img src="<?php echo get_bloginfo('template_url').'/images/ntrmds_icons/camera.svg'; ?>"><?php echo $prod_video_6['frontend_label'] ?></a></span> 
	<?php endif; ?>
<?php $sessionfee_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'sessionfee'); ?>
    <?php if($sessionfee_6['atr_value']) : 
	$sessionfee = implode(', ', $sessionfee_6['atr_value']); ?>
	<span class="bl bl-fee"><?php echo $sessionfee_6['frontend_label'] ?><span>:</span> <?php echo $sessionfee ?> </span> 
	<?php endif; ?> 
</div>
</div>
	</div>

<div class="right-info">
<?php // $schedule = get_post_meta($post->ID, 'schedule_session', true); 
$schedule_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'schedule_session'); ?>
    <?php if($schedule_6['atr_value']) : 
	$schedule = implode(', ', $schedule_6['atr_value']); ?>
 <a target="_blank" href="<?php echo $schedule ?>" class="btn-session"><?php echo $schedule_6['frontend_label'] ?></a> 
	<?php endif; ?>
</div>

<?php /* /// **** Група атрибутів 
<?php $my_group_attr_5 = WOW_Attributes_Front::post_view_my_group_attributes($post_id, 'feature'); ?>
            <?php if(count($my_group_attr_5)) : ?>
            <div class="attributes_51"> 
    <?php foreach ($my_group_attr_5 as $attribute) : ?> 
    <?php $value = implode(", ", $attribute['atr_value']); ?>
    <div class="my_atr_item <?php echo $attribute['code'] ?>">
    <a href="<?php echo $value ?>" target="_blank">
    <span class="lab"><?php echo $attribute['frontend_label'] ?> <span>:</span></span> <span class="value"><?php echo $value; ?><?php if($attribute['frontend_unit']) { ?> <span class="unit"><?php echo $attribute['frontend_unit'] ?></span><?php } ?></span>
     </a>
     </div>
     <?php endforeach; ?>    
            </div>
            <?php endif; ?>
 */ ?>
    
  
</div>  
</div> <!-- product-shop -->
  
    

       
   
<?php /* 
    <?php if(get_post_meta($post_id, 'prod_video', true)) : // $post_id / $post_id_gen 
	$video_short_code_1 = get_post_meta($post_id, 'prod_video', true);
	if(strpos($video_short_code_1, '[') !== false) { $video_short_code = $video_short_code_1; }
	else { $video_short_code = '[youtube]'.$video_short_code_1.'[/youtube]'; }
	?>
    <div class="box-content prod_video"> <?php echo do_shortcode($video_short_code); ?> </div>
	<?php endif; ?>
  */ ?>              
            

   

 
      <div class="box-content b-consultant maine">            	

<div class="wrap-base-consultant">
<?php // $short_descr = get_post_meta($post->ID, 'short_description', true); 
$specialist_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'specialist');
?>
    <?php if($specialist_6['atr_value']) : 
	$specialist = implode(', ', $specialist_6['atr_value']); ?>
    <div class="sp">
    <strong><?php echo $specialist_6['frontend_label'] ?></strong>
	<span><?php echo $specialist ?></span>
    </div>
	<?php endif; ?>
    
    
    <div class="entry-content"><?php the_content(); ?></div>
    
    
    <div class="wrap-btn-session">
<?php // $schedule = get_post_meta($post->ID, 'schedule_session', true); 
$schedule_6 = WOW_Attributes_Front::post_view_one_attribute($post_id, 'schedule_session'); ?>
    <?php if($schedule_6['atr_value']) : 
	$schedule = implode(', ', $schedule_6['atr_value']); ?>
 <a target="_blank" href="<?php echo $schedule ?>" class="btn-session btn-session-single"><?php echo $schedule_6['frontend_label'] ?></a> 
	<?php endif; ?> 
    </div>
</div>

      </div> 

              
</div> <!-- product-view -->
<?php /* **** __end wow_e_shop zone */ ?>



      
     <?php // Коментарі ?>   
     <?php // comments_template(); ?> 


<?php // -//- end main content ?> <?php endwhile; ?>	<?php else : ?>  	<?php endif; ?>






<?php /// Author latest posts

$post_id = $post->ID;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // pagination
$posts_args_2 = array (       
        'post_type'  => 'any',
		'posts_per_page'  => -1, // 6
		'paged' => $paged, // pagination
		'order' => 'DESC',	
		'orderby' => 'date',
		'post__not_in' => array( $post->ID ),
		'meta_query' => array(
			array (
			'key' => 'post_author_custom',
			'value' => $post_id, // array(2, 5) // (for 'BETWEEN')
			// 'compare' => '=', // '=', '!=', '>', '>=', '<', '<=', 'IN', 'NOT IN', 'BETWEEN', 
			)
		),
		'post_status' => 'publish'
    );

$author_query = new WP_Query($posts_args_2);
    if( $author_query->have_posts() ) { ?>
<div class="box-content maine ">
<?php $title_2 = get_the_title()."'s ".__('latest articles'); if(get_post_meta($post->ID, 'latest_posts_title', true)) { $title_2 = get_post_meta($post->ID, 'latest_posts_title', true); } ?>
<div class="tit"> <h3><?php echo $title_2 ?></h3> </div>
<div class="grid_cont">
<ul class="products-grid cols_3 blog-list "> 
<?php	
	while ($author_query->have_posts()) : 
	$author_query->the_post(); 
		// global $more;  $more = 0;  // необхідно для тегу <!--more-->
?>  
 <li class="item">
 <div class="inn">
<?php if ( has_post_thumbnail() ) { ?>
	<div class="thumbnail_5"> <a href="<?php the_permalink(); ?>" ><?php the_post_thumbnail( 'main-img' ); ?></a> </div>				
<?php } ?> 			
<h3> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a> </h3> 
<div class="ty">
 <?php $avtor_id = get_post_meta($post->ID, 'post_author_custom', true);
   $avtor_p_type = get_post_type($avtor_id);
   $avtor_name = get_the_title($avtor_id);  $avtor_link = get_the_permalink ($avtor_id); ?>
	<span class="author <?php echo $avtor_p_type; ?>"><em><?php _e('by') ?></em> <a href="<?php echo $avtor_link; ?>"><?php echo $avtor_name; ?></a></span>
	<span class="cat"><em><?php _e('in') ?></em><?php echo the_category(); ?></span>
</div>
<?php $cutti_num = 250;
// $short_content_2 = preg_replace('`\[[^\]]*\]`', '', strip_tags(get_the_content())); // 
$short_content_2 = strip_shortcodes( strip_tags(get_the_content()) ); // WP function "strip_shortcodes"
$charset = get_bloginfo('charset'); // $charset = 'UTF-8';
$short_content = mb_substr($short_content_2, 0, $cutti_num, $charset); 
$short_content = mb_substr($short_content, 0, strripos($short_content, ' ', 0, $charset), $charset);
?>
          <div class="descr entry-content"> <p><?php echo $short_content; ?></p> </div>
 </div>
 </li> 
<?php endwhile; ?>
</ul>
</div>
    <?php if (function_exists('wp_corenavi')) wp_corenavi($author_query); ?>
</div>
<?php }  wp_reset_query(); ?>


   
     

</div> <!-- content -->

	
 
 

</div> 













<?php /* wow_e_shop *** product contact_form *** */ ?>
<?php 
	$first_name = ''; $email = ''; $phone = '';
	if (is_user_logged_in()) {
	$current_user = wp_get_current_user();  $user_id = $current_user->id;
	$email = $current_user->user_email;
	$user_meta = get_user_meta($user_id);
	$first_name = $user_meta['first_name'][0]; 
	$phone = $user_meta['phone'][0];
	}
?>
<div class="lightb_window medium" id="lightb_contact_form_product" style="display: none;">
<a class="close_but btn-remove" onClick="overlay_hide()" > <i class="fa fa-close" aria-hidden="true"></i> </a>
<div class="lightb_inner"> 
<div class="contact-form product">
<h4><?php _e('Get more information about this product') ?></h4>
<?php $form_name = 'contact_form_product'; ?>
<form name="<?php echo $form_name ?>" id="<?php echo $form_name ?>" method="post">
<ul class="c_form fields">
<li> <label for="prod_customer_name"><?php _e('First Name') ?></label> <div class="box"><input type="text" name="customer_name" id="prod_customer_name" class="required" placeholder="<?php _e('First Name') ?>" ) ?>" value="<?php echo $first_name ?>" /></div> </li>
<li> <label for="prod_customer_phone"><?php _e('Phone') ?></label> <div class="box"><input type="text" name="customer_phone" id="prod_customer_phone" class="required" placeholder="<?php _e('Phone') ?>" value="<?php echo $phone ?>" /></div> </li>
<li> <label for="prod_customer_email"><?php _e('Email') ?></label> <div class="box"><input type="text" name="customer_email" id="prod_customer_email" placeholder="<?php _e('Email') ?>" value="<?php echo $email ?>" /></div> </li>
</ul>
<input type="hidden" name="product_id" value="<?php echo $post->ID ?>" />
<div class="but_line"><a class="button" onClick="do_contact_form('<?php echo $form_name ?>')"><span><?php _e('Submit') ?></span></a></div>
</form>
</div>
</div>
</div>







<?php get_footer(); ?>