<?php
/*
Template Name: WOW contacts
*/
?>


<?php get_header(); ?>

        
<div class="page contacts blog contacts">


    <?php // Лівий сайдбар ?>
    <?php // include 'column-left.php'; ?>

     
    
    <div class="content ajax_replace2_content">
      
     <?php // breadcrumbs
   //if (function_exists('breadcrumbs')) breadcrumbs(); ?>
   
   
   <?php // main content ?> <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
  
    <div class="page_title title_content"> <h1><?php the_title(); ?></h1> </div>
    
   <div class="box-content conte maine">
    <div class="entry-content"> <?php the_content(); ?> </div>
    <div id="contacts_page" class="contact-form contact maine">  
<?php if ( $post->post_excerpt ) { ?> <div class="form_title"><?php the_excerpt(); ?></div> <?php } ?>
<?php 
  $first_name = ''; $email = ''; $phone = '';
  if (is_user_logged_in()) {
  $current_user = wp_get_current_user();  $user_id = $current_user->id;
  $email = $current_user->user_email;
  $user_meta = get_user_meta($user_id);
  $first_name = $user_meta['first_name'][0]; 
  $phone = $user_meta['phone'][0];
  }
?>
<form name="contact_form" id="contact_form" enctype="multipart/form-data" action="<?php bloginfo('url'); echo '/contact-form-success/'; ?>" method="post">
<ul class="c_form fields">
<li> <label for="customer_name"><?php _e('Name:') ?></label> <div class="box"><input type="text" name="customer_name" id="customer_name" class="required" placeholder="" value="<?php echo $first_name ?>" /><p>Please enter your name</p></div> </li>
<li> <label for="customer_email"><?php _e('Email:') ?></label><div class="box"><input type="text" name="customer_email" id="customer_email" class="required" placeholder="" value="" /><p>Please enter a valid email address</p></div> </li>
<li> <label for="customer_phone"><?php _e('Subject:') ?></label> <div class="box"><input type="text" name="customer_subject" id="customer_subject" class="required" placeholder="" value="<?php echo $subject ?>" /><p>Please enter a subject</p></div> </li>

<li class="wide"> <!-- <label for="c_form_comment"><?php _e('Message') ?></label> --> <div class="box"><textarea name="comment" id="c_form_comment" class="required" placeholder="<?php _e('Message') ?>"></textarea><p>Please enter your message</p></div> </li>
</ul>
<div class="but_line"><a class="button" onClick="do_contact_form_2('')"><span><?php _e('Send') ?></span></a></div>
</form>

<?php /* 
customer_site
customer_city
customer_address

contact_form
contact_form_call_me
contact_form_product
*/ ?>




<?php /* 
<li> 
<label for="add_file"><?php _e('Add file') ?></label> 
<div class="box"> <div class="styleFileInput"> <input type="text" id="fileinput_text" class="browseText" readonly="readonly" /> <a class="button browseButton"><?php _e('Browse') ?></a> <input type="file" name="add_file" id="add_file" size="1" class="theFileInput" onchange="fileinput(this.value)" /> </div> </div> 
</li>
<script type="text/javascript">
        function fileinput(fName) {
      fullName = fName;
shortName = fullName.match(/[^\/\\]+$/);            
            // alert (shortName);      
       var input2 = document.getElementById ("fileinput_text");
       input2.value = shortName;        
        }
</script>
<style>
.styleFileInput {
    position: relative;
  display: inline-block;
  text-align: left;
}

.button.browseButton {
  padding: 0 8px;
    background: #a7851c;
    font-size: 0.9em;
    color: #fff;
    border: 1px solid #b1902c;
    border-radius: 4px;    
}

ul.fields input[type="text"].browseText {
    width: 190px;
    margin: 0 15px 0 0;
    padding: 2px 4px;
  font-size: 12px;
}

input.theFileInput {
    position:absolute; z-index:2;
    top: 0;
    right: 0;       
    width: 80px;
    font-size: 1em;
  opacity: 0; filter: alpha(opacity=0); 
}
</style>
*/ ?>




    </div>
   </div>  
   <?php // -//- end main content ?> <?php endwhile; ?>	<?php else : ?>  	<?php endif; ?>	    



	<?php /* Google Map */ ?>
    <?php if(get_post_meta($post->ID, 'location_map', true)) : // $post_id / $post_id_gen 
$map_field = get_post_meta($post->ID, 'location_map', true);
$map_coords_2 = explode('||', $map_field);
$map_coords = array('address' => $map_coords_2[2], 'lat' => $map_coords_2[0], 'lng' => $map_coords_2[1]);
	?>
<div class="box-content my_map">
<div class="tit map_title"><?php echo $map_coords['address']; ?></div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<div id="my_location_map" style="width: 100%; height: 400px;"></div>
<script type="text/javascript">
function google_map_load() {
	var lat = <?php echo $map_coords['lat']; ?>;
	var lng = <?php echo $map_coords['lng']; ?>;
	var latlng = new google.maps.LatLng(lat, lng);
	var myOptions = {
	zoom: 14,
	center: latlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP
   };

	var map = new google.maps.Map(document.getElementById("my_location_map"), myOptions);
	var marker = new google.maps.Marker({
	position: map.getCenter(),
	map: map
   });
}

google_map_load();
</script>
</div>
	<?php endif; // __ Google Map ?>
    
    

    

           
    </div>      
	

     
  
</div> <!-- class="page blog" -->



<?php get_footer(); ?>