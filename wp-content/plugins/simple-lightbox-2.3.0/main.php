<?php
/* 
Plugin Name: Simple Lightbox
Plugin URI: #
Description: The highly customizable lightbox for WordPress
Version: 20001.3.0
Author: Archetyped
Author URI: #
Support URI: #
*/
/*
Copyright 2014 Sol Marchessault (sol@archetyped.com)
*/
$slb = null;
/**
 * Initialize SLB
 */
function slb_init() {
	$path = dirname(__FILE__) . '/';
	require_once $path . 'load.php';
}

add_action('init', 'slb_init', 1);