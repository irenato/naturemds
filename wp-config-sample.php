<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/*
define('WP_HOME','http://drukart-2');
define('WP_SITEURL','http://drukart-2');
  */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nature_new_db');

/** MySQL database username */
define('DB_USER', 'nature_new_db');

/** MySQL database password */
define('DB_PASSWORD', 'CgeT59o~Z7a[');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/** Заборонити REVISIONS */
define('AUTOSAVE_INTERVAL', 17000 ); // seconds
define('WP_POST_REVISIONS', false );

/** Заборонити оновлення */
define( 'AUTOMATIC_UPDATER_DISABLED', true );

/* // Заборонити зміни файлів у адмінці, установку плагінів
define( 'DISALLOW_FILE_MODS', true );
 */

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D(2=.Ymq?Xai<8M]miG|TJfPZ0ZsTbHv,vTr&#mInjrs``2<^( ovD~uB%t`(KX8');
define('SECURE_AUTH_KEY',  'z/yOpqkzGyS6ShCCqV9Z!HLGg[$}:;B/roQtUm[+c8SYWmT1fO-TmG@r&Mul<`|[');
define('LOGGED_IN_KEY',    'f]F6,ZQ1`TjKP~wJ_~}:^Vsp#{F{M-CQj/{,ZL~ZEX` o?4!=FH-R6XP7OZWz`)~');
define('NONCE_KEY',        '37$nY)+?sc{D2rFz2&XlMpMNUajgmJ[]<!]u59!-cxxy_}&igkTgb~Sp[kLSa73#');
define('AUTH_SALT',        '%9xn%n^:e)V?A[[XLi<@r[vT$<X+ ;mWQ^,Z|)2>=q|h@ZGpr$kPHU],+ifH Zr{');
define('SECURE_AUTH_SALT', 's&]0Q*)zy.~_> l|{)kjx+/(f{Uh&:d7{tsFZEI<]+K|p>LocIdF]S4({9TXSa`v');
define('LOGGED_IN_SALT',   'ItV04B)XWa Ry<QRw~4kj$(b;v::f1N}r|)At.[,],pd&|Ori}Jt0}%w`I^/ylZ>');
define('NONCE_SALT',       '9}3,S.TTHP@SrvyTEXq|EJ3s4mMaf++$|/DP7Ue..@ggmJtogBmz<HY+g64K-aXV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

define( 'DISALLOW_FILE_EDIT', true );

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');